package com.ctospace.jdt.test;

import com.ctospace.core.classloader.XClassLoader;

public class TestClassLoader extends ClassLoader implements XClassLoader {
	
	public Class<?> addClass(String className, byte[] classData) {
		return this.defineClass(className, classData, 0, classData.length);
	}

}
