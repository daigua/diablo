package com.ctospace.jdt.test;

import japa.parser.JavaParser;
import japa.parser.ast.CompilationUnit;

import java.io.FileInputStream;

import org.junit.Test;

import com.ctospace.jdt.script.VarVisitor;

public class AstParserTest {

	@Test
	public void testGoogle() throws Exception {
		FileInputStream in = new FileInputStream("D:\\my-workspace\\diablo\\jdt\\src\\test\\java\\com\\ctospace\\jdt\\test\\TestAst.java");
		long cur = System.currentTimeMillis();
		CompilationUnit cu = JavaParser.parse(in);
		VarVisitor visitor = new VarVisitor();
		visitor.visit(cu, null);
		System.out.println(visitor.getVarDefs());
		System.out.println("共耗时：" + (System.currentTimeMillis() - cur));

	}
	
}
