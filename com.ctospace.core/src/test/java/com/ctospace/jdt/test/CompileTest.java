package com.ctospace.jdt.test;


import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.nio.charset.Charset;

import org.junit.Test;

import com.ctospace.jdt.compiler.JavaClass;
import com.ctospace.jdt.compiler.CompileHelper;
import com.ctospace.jdt.compiler.JavaSource;
import com.ctospace.jdt.script.Script;

public class CompileTest {

	@Test
	public void testCompile() {
		String HALLO_WORLD_CLASS_NAME = "com.ctospace.script.HelloWorldTest";
		String HALLO_WORLD_SOURCE = "package com.ctospace.script; \n"
				+ "	public class HelloWorldTest{\n"
				+ "    public static void main(String[] args) {\n"
				+ "        System.out.println(\"你好\");\n" + "    }\n" + "}";
		CompileHelper compilier = CompileHelper.getInstance();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PrintWriter out = new PrintWriter(bos);
		try {
			JavaSource scriptJava = new JavaSource(HALLO_WORLD_CLASS_NAME,
					HALLO_WORLD_SOURCE, Charset.forName("utf-8"));
			JavaClass clazzFileObject = compilier.compile(scriptJava, null, out);
			TestClassLoader cl = new TestClassLoader();
			Class<?> clazz = cl.addClass(HALLO_WORLD_CLASS_NAME,
					clazzFileObject.getBytes());
			Method method = clazz.getMethod("main", String[].class);
			method.invoke(null, new Object[] { new String[] {} });
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(new String(bos.toByteArray()));
	}

	@Test
	public void testScript() throws InstantiationException, IllegalAccessException {
		TestClassLoader classLoader = new TestClassLoader();
		Script script = new Script(classLoader);
		script.execute("int a = 1234; System.out.println(a);");
		String input = "import java.util.Date;";
		script.execute(input);
		input = "Date date = new Date();System.out.println(date);";
		script.execute(input);
		input = "import java.util.List;import java.util.ArrayList;\n" +
				"List<String> list = new ArrayList<String>();\n" +
				"list.add(\"A\");\n" +
				"list.add(\"B\");\n" +
				"System.out.println(list);";
		script.execute(input);
		input = "for(int i = 0; i < 3; i ++){System.out.println(i+\" =index=\");}";
		script.execute(input);
		
	}
	
	@Test
	public void testScriptClass(){
		TestClassLoader classLoader = new TestClassLoader();
		Script script = new Script(classLoader);
		//测试class
		String javaSource = "package com.ctospace.jdt.script;\n" +
				"public class TestClassScript{\n" +
				"	private String name;\n" +
				"	public void setName(String name){\n" +
				"		this.name = name;" +
				"	}\n" +
				"	public String getName(){\n" +
				"		return this.name;" +
				"	}\n" +
				"}";
		script.execute(javaSource);
		String input = "import com.ctospace.jdt.script.TestClassScript;\n" +
				"TestClassScript tcs = new TestClassScript();\n" +
				"tcs.setName(\"luling\");\n";
		script.execute(input);	
		input = "System.out.println(tcs.getName());";
		script.execute(input);
	}
	
	@Test
	public void testMethod(){
		TestClassLoader classLoader = new TestClassLoader();
		Script script = new Script(classLoader, "E:\\javawork\\diablo\\com.ctospace.core\\target\\classes");
		String input = "public void testMethod1(String say){System.out.println(\"m1:\" + say);}";
		script.execute(input);
		script.execute("testMethod1(\"123456\");");
		input = "public void testMethod2(String say){System.out.println(\"m2:\" + say);}";
		script.execute(input);
		script.execute("testMethod1(\"12345\");\ntestMethod2(\"345678\");");
		script.execute("String abc = \"abcdefghijklmn\";");
		script.execute("testMethod1(abc);\ntestMethod2(abc);");
	}
	
	@Test
	public void testClassLoader(){
		String className = "com.ctospace.jdt.script.CompileTest";
		TestClassLoader cl = new TestClassLoader();
		try {
			Class<?> clazz = cl.loadClass(className);
			System.out.println(clazz);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}











