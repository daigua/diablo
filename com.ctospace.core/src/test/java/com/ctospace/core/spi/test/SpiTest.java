package com.ctospace.core.spi.test;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ctospace.core.spi.bean.BeanFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring.xml")
public class SpiTest {
	
	private final static Logger logger = LoggerFactory.getLogger(SpiTest.class);
	
	@Resource
	private BeanFactory beanFactory;
	
	@Test
	public void test(){
		Bean bean = beanFactory.getBean(Bean.class);
		logger.info(bean.getId()+"");
		Assert.assertEquals(3, bean.getId());
	}
	
	
}
