package com.ctospace.core.spi.test;

import com.ctospace.core.spi.ProviderPriority;
import com.ctospace.core.spi.annotation.ServiceProvider;
import com.ctospace.core.spi.bean.BeanContext;

@ServiceProvider(value=BeanContext.class,priority=ProviderPriority.HIGH)
public class TestBeanContext1 implements BeanContext {

	@Override
	public <T> T getBean(String key) {
		return (T) new Bean(1);
	}

	@Override
	public <T> T getBean(Class<T> clazz) {
		if(clazz == Bean.class)
			return (T) new Bean(3);
		return null;
	}

}
