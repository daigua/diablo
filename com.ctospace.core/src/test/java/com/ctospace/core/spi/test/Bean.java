package com.ctospace.core.spi.test;

public class Bean {
	
	public Bean(int i){
		this.id = i;
	}

	private int id ;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
