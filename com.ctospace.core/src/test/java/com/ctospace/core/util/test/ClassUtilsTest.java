package com.ctospace.core.util.test;

import junit.framework.Assert;

import org.apache.commons.lang3.ClassUtils;
import org.junit.Test;

public class ClassUtilsTest {

	@Test
	public void test(){
		Class<TestChildTest> child = TestChildTest.class;
		Class<TestClass> tc = TestClass.class;
		Class<TestClassInterface> tci = TestClassInterface.class;
		Assert.assertTrue(ClassUtils.isAssignable(child, tc));
		Assert.assertTrue(ClassUtils.isAssignable(child, tci));
		Assert.assertFalse(ClassUtils.isAssignable(tc, child));
		Assert.assertFalse(ClassUtils.isAssignable(tci, child));
		Assert.assertFalse(ClassUtils.isAssignable(tc, tci));
		Assert.assertFalse(ClassUtils.isAssignable(tci, tc));
	}
	
}
