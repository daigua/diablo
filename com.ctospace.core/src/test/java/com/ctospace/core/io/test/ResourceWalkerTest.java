package com.ctospace.core.io.test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctospace.core.io.Resource;
import com.ctospace.core.io.ResourceVisitor;
import com.ctospace.core.io.ResourceWalker;
import com.ctospace.core.io.ResourceWalkerFactory;

public class ResourceWalkerTest {
	
	private Logger logger = LoggerFactory.getLogger(ResourceWalkerTest.class);

	@Test
	public void testWalkerFactory(){
		logger.info("测试 classpath路径");
		String path = "classpath:org/apache/**/*.class";
		ResourceWalker<Resource> walker = ResourceWalkerFactory.getResourceWalker(path);
		ResourceVisitor<Resource> visitor = new ResourceVisitor<Resource>(){

			@Override
			public void visit(Resource resource) {
				logger.info(resource.getPath());
			}
			
		};
		walker.walk(visitor);
		logger.info("测试文件路径");
		path = "file://E:/msmeik/msmeik/**/*.js";
		walker = ResourceWalkerFactory.getResourceWalker(path);
		walker.walk(visitor);
		logger.info("测试web目录");
		path = "E:/javawork/ctospace/com.ctospace.core.test/target/com.ctospace.core.test-0.0.1-SNAPSHOT/WEB-INF/**/*.xml";
		walker = ResourceWalkerFactory.getResourceWalker(path);
		walker.walk(visitor);
		
	}
	
}
