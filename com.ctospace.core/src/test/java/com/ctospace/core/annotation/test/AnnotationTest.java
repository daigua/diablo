package com.ctospace.core.annotation.test;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Iterator;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctospace.core.annotation.AnnotationContext;
import com.ctospace.core.annotation.AnnotationDefinition;


public class AnnotationTest {
	
	private int init = 0;
	
	private final static Logger logger = LoggerFactory.getLogger(AnnotationTest.class);

	private String location = "com/ctospace";
	
	private String annotationPrefix = "com.ctospace";
	private AnnotationContext context;
	
	@Before
	public void init(){
		if(init != 0)
			return;
		context = new AnnotationContext();
		context.setBasePackages(location);
		context.setAnnotationPrefix(annotationPrefix);
		context.init();
		init = 1;
	}
	
	@Test
	public void testAllAnnotation(){
		annotationPrefix = null;
		Class<? extends Annotation> annotationType = Tree.class;
		Collection<AnnotationDefinition> defs = context.getAnnotationDefinitions(annotationType);
		logger.info("得到definition size : " + defs.size());
		Iterator<AnnotationDefinition> it = defs.iterator();
		while(it.hasNext()){
			AnnotationDefinition def = it.next();
			logger.info(def.getTargetClass().getName());
		}
	}
	
	@Test
	public void testAnnotationPrefix(){
		Class<? extends Annotation> annotationType = TreeNode.class;
		Collection<AnnotationDefinition> defs = context.getAnnotationDefinitions(annotationType);
		logger.info("得到definition size " + defs.size());
		Iterator<AnnotationDefinition> it = defs.iterator();
		while(it.hasNext()){
			AnnotationDefinition def = it.next();
			logger.info(def.getTargetClass().getName());
		}
	}
}
