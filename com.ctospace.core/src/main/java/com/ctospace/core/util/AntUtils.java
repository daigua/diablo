package com.ctospace.core.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public abstract class AntUtils {

	public final static Map<String, String[]> pathCache = new HashMap<String, String[]>();

	public static boolean matchPath(String antPath, String matchPath) {
		/**
		 * 根据matchPath的后缀名判断是全路径匹配还是字路径匹配 如果matchPath是以文件名结尾的，则是全路径，否则是字路径
		 */
		return matchPath(antPath, matchPath, isFilePath(matchPath) ? true
				: false);
	}

	public static boolean matchPath(String antPath, String matchPath,
			boolean holeMatch) {
		String[] regex = null;
		if (antPath.endsWith("/")) {
			antPath = antPath.substring(0, antPath.length() - 1);
		}
		if (!isFilePath(antPath)) {
			antPath += "/**";
		}
		if (pathCache.containsKey(antPath)) {
			regex = pathCache.get(antPath);
		} else {
			regex = parseAntPath(antPath);
			pathCache.put(antPath, regex);
		}
		if (matchPath.startsWith("/"))
			matchPath = matchPath.substring(1);
		if (matchPath.endsWith("/"))
			matchPath = matchPath.substring(0, matchPath.length() - 1);
		String[] subPath = null;
		/**
		 * 全路径匹配
		 * */
		if (holeMatch) {
			subPath = regex;
			return match(subPath, matchPath);
		} else {
			for (int i = 1; i <= regex.length; i++) {
				String[] _regex = Arrays.copyOf(regex, i);
				if (match(_regex, matchPath))
					return true;
			}
			return false;
		}

	}

	private static boolean match(String[] regex, String matchPath) {
		String regexStr = StringUtils.join(regex, "/");
		/**
		 * 如果path为 com/ctopsace/**\/*.class 转化成为正则表达式为
		 * com/ctospace/.*\/[^/]*\.class 这样是匹配不到
		 * com/ctospace/aaa.class的，所以将上述表达式转化成为
		 * com/ctospace/.*[^/]*.class 就可以匹配到 
		 * */
		regexStr = regexStr.replace(".*/", ".*");
		regexStr = "^" + regexStr + "$";
		return Pattern.matches(regexStr, matchPath);
	}

	private static boolean isFilePath(String path) {
		String end = path.substring(path.lastIndexOf("/") + 1);
		if (end.indexOf(".") != -1 && !end.startsWith(".")) {
			return true;
		}
		return false;
	}

	public static String getRootPath(String antPath) {
		if (antPath.indexOf("*") == -1) {
			return antPath;
		} else {
			antPath = antPath.substring(0, antPath.indexOf("*"));
			if (antPath.endsWith("/")) {
				return antPath;
			} else
				return antPath.substring(0, antPath.lastIndexOf("/") + 1);
		}
	}

	private static String[] parseAntPath(String antPath) {
		String[] sub = antPath.split("/");
		for (int i = 0; i < sub.length; i++) {
			String path = sub[i];
			if (path.equals("**")) {
				sub[i] = ".*";
			} else
				sub[i] = cleanPath(path);
		}
		return sub;
	}

	private static String cleanPath(String path) {
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < path.length(); i++) {
			char c = path.charAt(i);
			switch (c) {
			case '*':
				b.append("[^/]*");
				break;
			case '\\':
				b.append("\\\\");
				break;
			case '.':
			case '-':
			case '[':
			case ']':
				b.append("\\").append(c);
				break;
			case '?':
				b.append(".");
				break;
			default:
				b.append(c);
			}
		}
		return b.toString();
	}

	public static void main(String[] args) {
		String path = "com";
		String str = "com/ctospace/xxx/x";
		// System.out.println(AntUtil.matchPath(path, str));
		path = "com/ctospace/*/spring.xml";
		str = "com/ctospace";
		System.out.println(AntUtils.matchPath(path, str));
		path = "com/ctospace/**/spring-*.xml";
		str = "com/ctospace/yyy/aaa/spring-xxx.xml";
		System.out.println(AntUtils.matchPath(path, str));
		path = "com/**/entity/**/*.class";
		str = "com/ctospace/entity/aaa/aaa/a.class";
		System.out.println(AntUtils.matchPath(path, str));
		path = "com/";
		System.out.println(AntUtils.matchPath(path, str));
	}
}
