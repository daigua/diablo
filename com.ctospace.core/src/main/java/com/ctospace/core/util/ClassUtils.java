package com.ctospace.core.util;

import java.net.URL;

public abstract class ClassUtils extends org.apache.commons.lang3.ClassUtils{
	
	public static <T> T newInstance(Class<T> clazz){
		try {
			return clazz.newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static boolean isChildOf(Class<?> child, Class<?> parent){
		Class<?>[] parents = child.getClasses();
		for(Class<?> p : parents){
			if(p == parent)
				return true;
		}
		return false;
	}
	
	public static ClassLoader getClassLoader(){
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		return cl;
	}
	
	public static String getClassLoaderPath(ClassLoader classLoader){
		URL url = classLoader.getResource("");
		return URLUtils.getDecodeFile(url);
	}
	
	public static String getClassLoaderPath(){
		return getClassLoaderPath(ClassUtils.getClassLoader());
	}
	
	public static void main(String[] args){
		System.out.println(ClassUtils.getClassLoaderPath());
	}
}
