package com.ctospace.core.util;

public abstract class PathUtils {
	
	public final static String WINDOWS_FILE_PROTOCOL = "file:/";
	
	/**
	 * 将文件路径转化问标准的路径格式，主要用于处理windows文件路径，即将E:\aaa\aaa.jsp转化成为E:/aaa/aaa.
	 * jsp格式用于统一处理，去除路径中以/结尾的/
	 * */
	public static String cleanPath(String path) {
		path = path.replace("\\", "/");
		path = path.replace("//", "/");
		/**
		 * 处理windows文件路径
		 * */
		if(path.startsWith(WINDOWS_FILE_PROTOCOL)){
			path = path.substring(WINDOWS_FILE_PROTOCOL.length());
		}
		if (path.endsWith("/"))
			path = path.substring(0, path.length() - 1);
		return path;
	}
}
