package com.ctospace.core.util.reflect;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.bcel.classfile.ClassFormatException;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariableTable;

import com.ctospace.core.util.ClassUtils;
import com.ctospace.core.util.StringUtils;

public abstract class MethodUtils {

	public static Object invoke(Method method, Object target, Object... params) {
		try {
			return method.invoke(target, params);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Method[] getMethods(Class<?> clazz, Class<? extends Annotation> annotationType){
		Method[] methods = clazz.getMethods();
		List<Method> filted = new ArrayList<Method>();
		for(Method method : methods){
			Annotation annotation = method.getAnnotation(annotationType);
			if(annotation != null){
				filted.add(method);
			}
		}
		return filted.toArray(new Method[filted.size()]);
	}

	public static Method getGetterMethod(Class<?> clazz, String name,
			Class<?>... parameterTypes) {
		String methodName = "get" + StringUtils.capitalize(name);
		return getMethod(clazz, methodName, parameterTypes);
	}

	public static Method getSetterMethod(Class<?> clazz, String name,
			Class<?>... parameterTypes) {
		String methodName = "set" + StringUtils.capitalize(name);
		return getMethod(clazz, methodName, parameterTypes);
	}

	public static Method getMethod(Class<?> clazz, String methodName,
			Class<?>[] parameterTypes) {
		try {
			return clazz.getMethod(methodName, parameterTypes);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	public static Object invoke(Class<?> clazz, String methodName,
			Class<?>[] parameterTypes, Object target, Object... params) {
		try {
			Method method = getMethod(clazz, methodName, parameterTypes);
			return method.invoke(target, params);
		} catch (SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}

	}

	public static String[] getMethodParameterNames(Method method) {
		Class<?> declaringClass = method.getDeclaringClass();
		int paramLength = method.getParameterTypes().length;
		InputStream is = ClassUtils.getClassLoader()
				.getResourceAsStream(
						declaringClass.getName().replaceAll("\\.", "/")
								+ ".class");
		ClassParser parser = new ClassParser(is, declaringClass.getSimpleName()
				+ ".class");

		try {
			JavaClass clazz = parser.parse();
			org.apache.bcel.classfile.Method bcelMethod = clazz
					.getMethod(method);
			LocalVariableTable variableTable = bcelMethod
					.getLocalVariableTable();
			String[] names = new String[paramLength];
			for(int i = 0; i < paramLength; i++){
				names[i] = variableTable.getLocalVariable(i+1).getName();
			}
			return names;
		} catch (ClassFormatException | IOException e) {
			throw new RuntimeException(e);
		}

	}

	public static void main(String[] args) {
		try {
			Method method = MethodUtils.class.getMethod("invoke", new Class<?>[] {
					Class.class, String.class, Class[].class, Object.class,
					Object[].class });
			String[] names = getMethodParameterNames(method);
			System.out.println(Arrays.asList(names));
		} catch (SecurityException | NoSuchMethodException e) {
			e.printStackTrace();
		}

	}

}
