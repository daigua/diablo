package com.ctospace.core.util;

public abstract class StringUtils extends org.apache.commons.lang3.StringUtils{

	public static String join(String[] strings, String split){
		if(strings == null)
			return null;
		StringBuffer buf = new StringBuffer();
		split = split == null ? "" : split;
		int flag = 0;
		for(String _str : strings){
			if(flag == 1){
				buf.append(split);
			}else{
				flag = 1;
			}
			buf.append(_str);
		}
		return buf.toString();
	}
	
}
