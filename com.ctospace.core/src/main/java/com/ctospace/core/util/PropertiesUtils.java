package com.ctospace.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtils {
	
	public static Properties get(InputStream is){
		if(is == null)
			return null;
		Properties properties = new Properties();
		try {
			properties.load(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return properties;
	}
	
	public static Properties getFromClassPath(String path){
		InputStream is = ClassUtils.getClassLoader().getResourceAsStream(path);
		return get(is);
	}
	
	public static Properties merge(Properties defaultProperties, Properties properties){
		Properties merged = new Properties();
		
		for(Object key : defaultProperties.keySet()){
			if(properties.containsKey(key)){
				merged.put(key, properties.get(key));
			}else{
				merged.put(key, defaultProperties.get(key));
			}
		}
		return merged;
	}
	
}
