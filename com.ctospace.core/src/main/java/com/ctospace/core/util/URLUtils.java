package com.ctospace.core.util;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

public class URLUtils {
	
	public final static String DEFAULT_CHARSET = "utf-8";

	public static String getDecodeFile(URL url){
		return getDecodeFile(url, DEFAULT_CHARSET);
	}
	
	public static String getDecodeFile(URL url, String charset){
		try {
			return URLDecoder.decode(url.getFile(), DEFAULT_CHARSET);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
}
