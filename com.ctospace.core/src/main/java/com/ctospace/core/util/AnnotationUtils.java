package com.ctospace.core.util;

import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Set;

public abstract class AnnotationUtils {

	/**
	 * 返回clazz上的以annotationPrefix开头的annotation数组, 如果annotationPrefix为null，则返回所有
	 * */
	public static Annotation[] getAnnotation(Class<?> clazz,
			String annotationPrefix) {
		Annotation[] annotations = clazz.getAnnotations();
		if (annotations.length == 0)
			return new Annotation[0];
		Set<Annotation> tmp = new HashSet<Annotation>();
		for (Annotation annotation : annotations) {
			String typeName = annotation.annotationType().getName();
			if (annotationPrefix == null
					|| typeName.startsWith(annotationPrefix)) {
				tmp.add(annotation);
			}
		}
		return tmp.toArray(new Annotation[tmp.size()]);
	}

	/**
	 * get the Annotation whose type equals to annotationType in annotations 
	 * */
	@SuppressWarnings("unchecked")
	public static <T extends Annotation> T getAnnotation(Annotation[] annotations,
			Class<T> annotationType) {
		for(Annotation annotation : annotations){
			if(annotation.annotationType() == annotationType)
				return (T) annotation;
		}
		return null;
	}

}
