package com.ctospace.core.io;

import java.io.File;

public interface FileResource<R extends Resource> extends ListAbleResource<R>, Resource {

	public File getFile();
	
	public boolean isFile();
	
	public boolean isDirectory();
	
}
