package com.ctospace.core.io.classpath;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.ctospace.core.util.ClassUtils;

public class JarEntryResource implements ClassPathResource {
	
	private JarFile jarFile;
	
	private JarEntry jarEntry;
	
	protected ClassLoader classLoader;
	
	public JarEntryResource(JarFile jarFile, JarEntry jarEntry){
		this(ClassUtils.getClassLoader(), jarFile, jarEntry);
	}
	
	public JarEntryResource(ClassLoader classLoader, JarFile jarFile, JarEntry jarEntry){
		this.jarEntry = jarEntry;
		this.jarFile = jarFile;
		this.classLoader = classLoader;
	}

	public InputStream getInputStream() {
		try {
			return jarFile.getInputStream(jarEntry);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public ClassLoader getClassLoader() {
		return this.classLoader;
	}

	public void setClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

	public boolean isClassResource() {
		return jarEntry.getName().endsWith(".class");
	}

	public Class<?> getClazz() {
		if(!isClassResource()){
			throw new RuntimeException(" This resource is not a class resource");
		}
		try {
			return Class.forName(getClassName());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public String getClassName() {
		String classname = getPath().replace("/", ".");
		classname = classname.substring(0, classname.length() - 6);
		return classname;
	}

	public String getPath() {
		return jarEntry.getName();
	}

	public String getFullPath() {
		return jarEntry.getName();
	}

	@Override
	public boolean isFile() {
		return !this.jarEntry.isDirectory();
	}

}
