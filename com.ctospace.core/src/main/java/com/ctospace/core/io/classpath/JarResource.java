package com.ctospace.core.io.classpath;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.ctospace.core.io.ResourceListFilter;
import com.ctospace.core.io.classpath.JarEntryResource;
import com.ctospace.core.util.URLUtils;

public class JarResource extends AbstractClassPathResource<JarEntryResource> implements ClassPathResource{

	public JarResource(URL url) {
		super(url);
	}
	
	public JarResource(ClassLoader classLoader, URL url){
		super(classLoader, url);
	}
	
	public JarResource(File file) {
		super(file);
	}
	
	public JarResource(ClassLoader classLoader, File file){
		super(classLoader, file);
	}

	public List<JarEntryResource> listResource() {
		return listResource(null);
	}

	public List<JarEntryResource> listResource(
			ResourceListFilter<JarEntryResource> filter) {
		try {
			List<JarEntryResource> jers = new ArrayList<JarEntryResource>();
			JarURLConnection conn = (JarURLConnection) url.openConnection();
			JarFile jarFile = conn.getJarFile();
			Enumeration<JarEntry> je = jarFile.entries();
			while(je.hasMoreElements()){
				JarEntry jarEntry = je.nextElement();
				JarEntryResource resource = new JarEntryResource(this.classLoader, jarFile, jarEntry);
				if(filter == null || filter.filter(resource))
					jers.add(resource);
			}
			return jers;
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}

	public ClassLoader getClassLoader() {
		return this.classLoader;
	}

	public void setClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

	public String getLoadPath() {
		throw new RuntimeException("JarResource don't have a load path");
	}

	public String getFullPath() {
		return URLUtils.getDecodeFile(url);
	}

	public boolean isClassResource() {
		return false;
	}

	public Class<?> getClazz() {
		throw new RuntimeException("This is a jar resource, not a class resource");
	}

	public String getClassName() {
		throw new RuntimeException("This is a jar resource, not a class resource");
	}

	public String getPath() {
		return URLUtils.getDecodeFile(url);
	}

}
