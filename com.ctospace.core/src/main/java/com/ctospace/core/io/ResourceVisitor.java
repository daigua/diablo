package com.ctospace.core.io;

public interface ResourceVisitor<T extends Resource> {
	
	public void visit(T resource);
	
}
