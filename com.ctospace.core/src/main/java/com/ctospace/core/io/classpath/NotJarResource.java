package com.ctospace.core.io.classpath;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.ctospace.core.io.ResourceListFilter;
import com.ctospace.core.util.ClassUtils;

public class NotJarResource extends AbstractClassPathResource<NotJarResource> {

	public NotJarResource(ClassLoader classLoader, String filePath) {
		super(classLoader, filePath);
	}
	
	public NotJarResource(String filePath){
		this(ClassUtils.getClassLoader(), filePath);
	}
	
	public NotJarResource(ClassLoader classLoader, URL url) {
		super(classLoader, url);
	}
	
	public NotJarResource(URL url){
		super(url);
	}
	
	public NotJarResource(ClassLoader classLoader, File file) {
		super(file);
		this.classLoader = classLoader;
	}
	
	public NotJarResource(File file){
		this(ClassUtils.getClassLoader(), file);
	}

	public boolean isClassResource() {
		return this.filePath.endsWith(".class");
	}

	public Class<?> getClazz() {
		try {
			return Class.forName(getClassName());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public String getClassName() {
		String classname = getPath().replace("/", ".");
		classname = classname.substring(0, classname.length() - 6);
		return classname;
	}

	public List<NotJarResource> listResource() {
		return listResource(null);
	}

	public List<NotJarResource> listResource(
			ResourceListFilter<NotJarResource> filter) {
		if(this.isFile()){
			throw new RuntimeException("This is a file resource ,not a directory resource");
		}
		List<NotJarResource> cprs = new ArrayList<NotJarResource>();
		for(File file : this.getFile().listFiles()){
			NotJarResource njr = new NotJarResource(this.classLoader, file);
			if(filter == null || filter.filter(njr)){
				cprs.add(njr);
			}
		}
		return cprs;
	}

	public String getPath() {
		return this.filePath.substring(ClassUtils.getClassLoaderPath(classLoader).length() - 1).replace("\\", "/");
	}


}
