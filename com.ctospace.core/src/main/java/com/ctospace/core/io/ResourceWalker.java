package com.ctospace.core.io;

public interface ResourceWalker<R extends Resource> {

	public <T extends ResourceVisitor<R>> void walk(T visitor);
	
	public <T extends ResourceVisitor<R>> void walk(T visitor, ResourceListFilter<R> filter);
}
