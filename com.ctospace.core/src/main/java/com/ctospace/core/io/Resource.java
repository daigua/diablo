package com.ctospace.core.io;

import java.io.InputStream;

public interface Resource {
	
	/**
	 * 获取该Resource的输出流
	 * */
	public InputStream getInputStream();
	
	public String getPath();
	
}
