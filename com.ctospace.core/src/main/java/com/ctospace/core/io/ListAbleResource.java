package com.ctospace.core.io;

import java.util.List;

public interface ListAbleResource<T extends Resource> {
	
	public List<T> listResource();
	
	public List<T> listResource(ResourceListFilter<T> filter);
	
	public boolean canList();
	
	
}
