package com.ctospace.core.io.classpath;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import com.ctospace.core.io.ResourceListFilter;
import com.ctospace.core.io.ResourceVisitor;
import com.ctospace.core.io.ResourceWalker;
import com.ctospace.core.util.AntUtils;
import com.ctospace.core.util.ClassUtils;

public class ClassPathResourceWalker implements
		ResourceWalker<ClassPathResource> {

	private String path;

	private ClassLoader classLoader;

	public ClassPathResourceWalker(String path) {
		this(ClassUtils.getClassLoader(), path);
	}

	public ClassPathResourceWalker(ClassLoader classLoader, String path) {
		this.path = path;
		this.classLoader = classLoader;
	}

	public <T extends ResourceVisitor<ClassPathResource>> void walk(T visitor) {
		this.walk(visitor, null);
	}

	public <T extends ResourceVisitor<ClassPathResource>> void walk(T visitor,
			ResourceListFilter<ClassPathResource> filter) {
		try {
			String startPath = AntUtils.getRootPath(path);
			Enumeration<URL> e = classLoader.getResources(startPath);
			ResourceListFilter<JarEntryResource> jarEntryFilter = this
					.getResourceListFilter(filter);
			ResourceListFilter<NotJarResource> njrFilter = this
					.getResourceListFilter(filter);
			while (e.hasMoreElements()) {
				URL url = e.nextElement();
				String protocol = url.getProtocol();
				// 如果是jar文件
				if ("jar".equals(protocol)) {
					JarResource jarResource = new JarResource(url);
					this.walkJarResource(jarResource, visitor, jarEntryFilter);
				} else {
					// 如果是非jar文件，即File文件。
					NotJarResource njr = new NotJarResource(url);
					this.walkNotJarResource(njr, visitor, njrFilter);
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private <T extends ResourceVisitor<ClassPathResource>> void walkNotJarResource(
			NotJarResource njr, T visitor,
			ResourceListFilter<NotJarResource> filter) {
		List<NotJarResource> njrs = njr.listResource(filter);
		for (NotJarResource r : njrs) {
			if (r.canList()) {
				this.walkNotJarResource(r, visitor, filter);
			} else {
				visitor.visit(r);
			}
		}
	}

	private <T extends ResourceVisitor<ClassPathResource>> void walkJarResource(
			JarResource jarResource, T visitor,
			ResourceListFilter<JarEntryResource> filter) {
		List<JarEntryResource> jers = jarResource.listResource(filter);
		for (JarEntryResource jer : jers) {
			if(jer.isFile())
				visitor.visit(jer);
		}
	}

	private <T extends ClassPathResource> ResourceListFilter<T> getResourceListFilter(final ResourceListFilter<ClassPathResource> filter) {

		ResourceListFilter<T> _filter = new ResourceListFilter<T>() {
			public boolean filter(T resource) {
				if ((filter == null || filter.filter(resource)) && AntUtils.matchPath(path, resource.getPath())) {
					return true;
				}
				return false;
			}

		};
		return _filter;
	}

}
