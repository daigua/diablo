package com.ctospace.core.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public abstract class AbstractFileResource<T extends Resource> implements FileResource<T> {
	
	protected String filePath;
	
	protected File file;
	
	public AbstractFileResource(String filePath){
		this.filePath = filePath;
		this.file = new File(filePath);
	}
	
	public AbstractFileResource(File file){
		this.file = file;
		this.filePath = file.getPath();
	}

	public InputStream getInputStream() {
		try {
			return new FileInputStream(getFile());
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public File getFile() {
		return this.file;
	}

	public boolean isFile() {
		return file.isFile();
	}

	public boolean isDirectory() {
		return file.isDirectory();
	}

	public boolean canList() {
		return isDirectory();
	}

}
