package com.ctospace.core.io.filesystem;

import java.util.List;

import com.ctospace.core.io.ResourceListFilter;
import com.ctospace.core.io.ResourceVisitor;
import com.ctospace.core.io.ResourceWalker;
import com.ctospace.core.util.AntUtils;
import com.ctospace.core.util.PathUtils;

public class FileSystemResourceWalker implements
		ResourceWalker<FileSystemResource> {

	private String path;

	private String root;

	public FileSystemResourceWalker(String path) {
		path = PathUtils.cleanPath(path);
		root = AntUtils.getRootPath(path);
		this.path = path;
	}

	public <T extends ResourceVisitor<FileSystemResource>> void walk(T visitor) {
		this.walk(visitor, null);
	}

	public <T extends ResourceVisitor<FileSystemResource>> void walk(T visitor,
			ResourceListFilter<FileSystemResource> filter) {
		FileSystemResource resource = new FileSystemResource(root);
		filter = getFileSysteResourceFilter(filter);
		walkFileSystemResource(resource, visitor, filter);
	}

	private <T extends ResourceVisitor<FileSystemResource>> void walkFileSystemResource(FileSystemResource resource, T visitor,
			ResourceListFilter<FileSystemResource> filter) {
		if(filter != null && !filter.filter(resource))
			return;
		if(!resource.canList())
			visitor.visit(resource);
		else{
			List<FileSystemResource> children = resource.listResource(filter);
			for(FileSystemResource r : children)
				walkFileSystemResource(r, visitor, filter);
		}
			
	}
	
	private ResourceListFilter<FileSystemResource> getFileSysteResourceFilter(final ResourceListFilter<FileSystemResource> filter){
		ResourceListFilter<FileSystemResource> _filter = new ResourceListFilter<FileSystemResource>(){

			@Override
			public boolean filter(FileSystemResource resource) {
				if(AntUtils.matchPath(path, resource.getPath()) && (filter == null || filter.filter(resource)))
					return true;
				return false;
			}
			
		};
		return _filter;
	}
	
}
