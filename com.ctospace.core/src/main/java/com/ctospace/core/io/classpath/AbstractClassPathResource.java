package com.ctospace.core.io.classpath;


import java.io.File;
import java.net.URL;

import com.ctospace.core.io.AbstractFileResource;
import com.ctospace.core.util.ClassUtils;
import com.ctospace.core.util.URLUtils;

public abstract class AbstractClassPathResource<T extends ClassPathResource> extends AbstractFileResource<T> implements ClassPathResource{

	protected ClassLoader classLoader;
	
	protected URL url;
	
	public AbstractClassPathResource(ClassLoader classLoader, String filePath) {
		super(filePath);
		this.classLoader = classLoader;
	}
	
	public AbstractClassPathResource(ClassLoader classLoader, File file) {
		super(file);
		this.classLoader = classLoader;
	}
	
	public AbstractClassPathResource(URL url){
		this(ClassUtils.getClassLoader(), url);
	}
	
	public AbstractClassPathResource(ClassLoader classLoader, URL url) {
		super(URLUtils.getDecodeFile(url));
		this.url = url;
		this.classLoader = classLoader;
	}
	
	public AbstractClassPathResource(File file){
		this(ClassUtils.getClassLoader(), file);
	}
	
	public AbstractClassPathResource(String filePath){
		this(ClassUtils.getClassLoader(), filePath);
	}

	public ClassLoader getClassLoader() {
		return classLoader;
	}

	public void setClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

}
