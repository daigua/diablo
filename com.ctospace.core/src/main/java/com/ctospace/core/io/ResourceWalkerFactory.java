package com.ctospace.core.io;

import com.ctospace.core.io.classpath.ClassPathResourceWalker;
import com.ctospace.core.io.filesystem.FileSystemResourceWalker;
import com.ctospace.core.util.PathUtils;
import com.ctospace.core.web.ServletEnv;

public class ResourceWalkerFactory {
	
	public static final String CLASSPATH = "classpath:";
	
	public static final String FILESYSTEM_WINDOWS = "file://";
	
	/**
	 * 根据输入的路径格式自动选择相应的ResourceWalker
	 * 支持的文件路径格式，路径格式为ant格式：
	 * 		classpath:com/ctospace 
	 * 		file://E:/msmeik/**\/*.js 匹配windows文件系统的绝对路径
	 * 		E://msmeik/ 匹配windows文件系统的绝对路径
	 * 		/home/website/ 匹配linux文件系统的绝对路径
	 * 		WEB-INF/config/spring-*.xml 匹配web工程目录
	 * */
	@SuppressWarnings("unchecked")
	public static <T extends Resource> ResourceWalker<T> getResourceWalker(String path){
		path = PathUtils.cleanPath(path);
		ResourceWalker<? extends Resource> walker = null;
		/**
		 * classpath的路径
		 * */
		if(path.startsWith(CLASSPATH)){
			path = path.substring(CLASSPATH.length());
			walker = new ClassPathResourceWalker(path);
		}else
		/**
		 * 处理绝对路径文件
		 * file://E:/aaa  或者  /opt/lampp 
		 * */
		if(path.startsWith(FILESYSTEM_WINDOWS) || path.startsWith("/")){
			walker = new FileSystemResourceWalker(path);
		}else{
			/**
			 * 处理windows系统路径， 如E:/aaa
			 * */
			int i = path.indexOf(":");
			if(i != -1 && i < path.indexOf("/")){
				walker = new FileSystemResourceWalker(path);
			}
			/**
			 * 其他的看作是webroot的文件路径
			 * */
			else{
				String root = ServletEnv.getWebRoot();
				root = PathUtils.cleanPath(root);
				walker = new FileSystemResourceWalker(root+"/"+path);
			}
		}
		return (ResourceWalker<T>) walker;
	}
	
}
