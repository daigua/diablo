package com.ctospace.core.io.classpath;

import com.ctospace.core.io.Resource;

public interface ClassPathResource extends Resource, ClassResource{

	public ClassLoader getClassLoader();
	
	public void setClassLoader(ClassLoader classLoader);
	
	public boolean isFile();
	
}
