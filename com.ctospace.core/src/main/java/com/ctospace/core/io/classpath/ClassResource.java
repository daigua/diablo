package com.ctospace.core.io.classpath;

public interface ClassResource {
	
	public boolean isClassResource();
	
	public Class<?> getClazz();
	
	public String getClassName();
	
}
