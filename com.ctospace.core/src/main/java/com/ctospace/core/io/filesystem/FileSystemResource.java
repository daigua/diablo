package com.ctospace.core.io.filesystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.ctospace.core.io.FileResource;
import com.ctospace.core.io.ResourceListFilter;
import com.ctospace.core.util.PathUtils;

public class FileSystemResource implements FileResource<FileSystemResource> {
	
	private File file;
	
	private String path;
	
	/**
	 * file 该文件的绝对路径
	 * root 该文件的查找根路径
	 * */
	public FileSystemResource(String file){
		this(new File(file));
	}
	
	/**
	 * file 该文件
	 * root 该文件的查找根路径
	 * */
	public FileSystemResource(File file){
		this.file = file;
		String _path;
		try {
			_path = this.file.getCanonicalPath();
		} catch (IOException e) {
			_path = file.getPath();
		}
		_path = PathUtils.cleanPath(_path);
		this.path = _path;
	}

	public List<FileSystemResource> listResource() {
		return listResource(null);
	}

	public List<FileSystemResource> listResource(
			ResourceListFilter<FileSystemResource> filter) {
		if(!this.canList()){
			throw new RuntimeException(" it's a file resource,not a directory resource");
		}
		List<FileSystemResource> resources = new ArrayList<FileSystemResource>();
		for(File file : this.file.listFiles()){
			FileSystemResource resource = new FileSystemResource(file);
			if(filter == null || filter.filter(resource)){
				resources.add(resource);
			}
		}
		return resources;
	}

	public boolean canList() {
		return file.isDirectory();
	}

	public InputStream getInputStream() {
		try {
			return new FileInputStream(this.getFile());
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public String getPath() {
		return path;
	}

	public File getFile() {
		return file;
	}

	public boolean isFile() {
		return file.isFile();
	}

	public boolean isDirectory() {
		return file.isDirectory();
	}

}
