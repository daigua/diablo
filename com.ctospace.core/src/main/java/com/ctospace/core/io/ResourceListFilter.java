package com.ctospace.core.io;

public interface ResourceListFilter<T extends Resource> {

	public boolean filter(T resource);
	
}
