package com.ctospace.core.annotation;

import java.lang.annotation.Annotation;
import java.util.Collection;

/**
 * The interface for the purpose to processor the classes which announced by a
 * annotation after the AnnotationContext initialized, write a class implements
 * {@link PostInitializeProcessor} or extends
 * {@link AbstractPostInitializeProcessor} and announce {@link PostInitialize} on
 * the class
 * */
public interface PostInitializeProcessor<T extends Annotation> {

	/**
	 * process the AnnotationDefinition
	 * 
	 * @param defs
	 *            the definitions which annotered by the annotation type
	 * */
	public void process(Collection<AnnotationDefinition> defs);

	/**
	 * set the AnnotationContext, the you can use the annotationContext in your
	 * implement class
	 * */
	public void setAnnotationContext(AnnotationContext annotationContext);

}
