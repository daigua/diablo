package com.ctospace.core.annotation;

import java.lang.annotation.Annotation;

public abstract class AbstractPostInitializeProcessor<T extends Annotation>
		implements PostInitializeProcessor<T> {

	protected AnnotationContext annotationContext;

	@Override
	public void setAnnotationContext(AnnotationContext annotationContext) {
		this.annotationContext = annotationContext;
	}

	protected AnnotationContext getAnnotationContext() {
		return this.annotationContext;
	}

}
