package com.ctospace.core.annotation;

import java.lang.annotation.Annotation;
import java.util.Collection;


public class AnnotationContextPostInitializeProcessor {

	public void postInitalize(AnnotationContext annotationContext) {
		/**
		 * 找到所有的AnnotationPostProcessor
		 * */
		Collection<AnnotationDefinition> defs = annotationContext
				.getAnnotationDefinitions(PostInitialize.class);
		for (AnnotationDefinition def : defs) {
			PostInitialize annotation = def.getAnnotation();
			this.processAnnotation(annotationContext, def.getTargetClass(),
					annotation.value());
		}
	}

	private void processAnnotation(AnnotationContext annotationContext,
			Class<?> processType,
			Class<Annotation> processAnnotationType) {
		Collection<AnnotationDefinition> annotationDefinitions = annotationContext
				.getAnnotationDefinitions(processAnnotationType);
		try {
			@SuppressWarnings("unchecked")
			PostInitializeProcessor<Annotation> processor = (PostInitializeProcessor<Annotation>) processType
					.newInstance();
			processor.setAnnotationContext(annotationContext);
			processor.process(annotationDefinitions);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}

	}
}
