package com.ctospace.core.annotation;

import java.lang.annotation.Annotation;

/**
 * annotation definition of the clazz 
 * */
public class AnnotationDefinition {
	
	private Annotation annotation;
	
	private Class<?> targetClass;
	
	public <T extends Annotation> AnnotationDefinition(Class<?> targetClass, T annotation){
		this.annotation = annotation;
		this.targetClass = targetClass;
	}

	@SuppressWarnings("unchecked")
	public <T extends Annotation> T getAnnotation() {
		return (T) annotation;
	}

	public <T extends Annotation> void setAnnotation(T annotation) {
		this.annotation = annotation;
	}

	public Class<?> getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(Class<?> targetClass) {
		this.targetClass = targetClass;
	}

}
