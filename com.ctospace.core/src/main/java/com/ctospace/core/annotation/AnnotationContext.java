package com.ctospace.core.annotation;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ctospace.core.io.ResourceListFilter;
import com.ctospace.core.io.ResourceVisitor;
import com.ctospace.core.io.classpath.ClassPathResource;
import com.ctospace.core.io.classpath.ClassPathResourceWalker;
import com.ctospace.core.util.AnnotationUtils;

/**
 * Use this, you can easily get a collection of classes which it had been
 * annotered by a annotation
 * */
@Component("com.ctospace.core.annotation.AnnotationContext")
public class AnnotationContext {

	private String[] basePackages;

	@Value("${annotation.context.scan.prefix:}")
	private String annotationPrefix;
	
	private Logger logger = LoggerFactory.getLogger(AnnotationContext.class);

	private Map<Class<? extends Annotation>, Collection<AnnotationDefinition>> annotationMapping;

	public AnnotationContext() {
		annotationMapping = new HashMap<Class<? extends Annotation>, Collection<AnnotationDefinition>>();
	}
	
	@PostConstruct
	public void init() {
		logger.debug("initialize annotation context");
		for (String location : basePackages) {
			logger.debug("scan package : " + location);
			this.walkLocation(location);
		}
		this.postInitialize();
	}

	/**
	 * After the annotation context initialize
	 * */
	private void postInitialize() {
		AnnotationContextPostInitializeProcessor processor = new AnnotationContextPostInitializeProcessor();
		processor.postInitalize(this);
	}

	private void walkLocation(String location) {
		ClassPathResourceWalker walker = new ClassPathResourceWalker(location);

		ResourceListFilter<ClassPathResource> filter = new ResourceListFilter<ClassPathResource>() {

			@Override
			public boolean filter(ClassPathResource resource) {
				return !resource.isFile() || resource.isClassResource();
			}

		};

		ResourceVisitor<ClassPathResource> visitor = new ResourceVisitor<ClassPathResource>() {
			@Override
			public void visit(ClassPathResource resource) {
				logger.debug("visit class : " + resource.getClassName());
				addClass(resource.getClazz(), annotationPrefix);
			}
		};

		walker.walk(visitor, filter);
	}

	/**
	 * 添加class，会解析class上的标注，并且缓存标注与class的关系
	 * */
	public void addClass(Class<?> clazz) {
		addClass(clazz, null);
	}

	/**
	 * 清空AnnotationContext
	 * */
	public void empty() {
		annotationMapping.clear();
	}

	/**
	 * 添加class，根据class上的所有的annotation，加入到context中，
	 * 该方法会过滤掉非annotationPrefix开头的annotation
	 * 
	 * @param clazz
	 *            需要添加的class
	 * @param annotationPrefix
	 *            仅匹配该以该参数开头的annotation，如果该参数为null，则匹配所有的annotation
	 * */
	public void addClass(Class<?> clazz, String annotationPrefix) {
		Annotation[] annotations = AnnotationUtils.getAnnotation(clazz,
				annotationPrefix);
		for (Annotation annotation : annotations) {
			addAnnotation(annotation, clazz);
		}
	}

	private void addAnnotation(Annotation annotation, Class<?> clazz) {
		Class<? extends Annotation> annotationType = annotation
				.annotationType();
		synchronized (annotationType) {
			Collection<AnnotationDefinition> defs = annotationMapping
					.get(annotationType);
			if (defs == null) {
				defs = new HashSet<AnnotationDefinition>();
				annotationMapping.put(annotationType, defs);
			}
			defs.add(new AnnotationDefinition(clazz, annotation));
		}
	}

	public <T extends Annotation> Collection<AnnotationDefinition> getAnnotationDefinitions(
			Class<T> annotationType) {
		Collection<AnnotationDefinition> defs = annotationMapping
				.get(annotationType);
		return defs == null ? new HashSet<AnnotationDefinition>() : defs;
	}

	public String[] getBasePackages() {
		return basePackages;
	}

	/**
	 * packages to scan
	 * */
	public void setBasePackages(String[] basePackages) {
		this.basePackages = basePackages;
	}

	public String getAnnotationPrefix() {
		return annotationPrefix;
	}

	/**
	 * The prefix of the annotation
	 * */
	public void setAnnotationPrefix(String annotationPrefix) {
		this.annotationPrefix = annotationPrefix;
	}

	/**
	 * packages to scan, more packages use ',' to split
	 * */
	@Value("${annotation.context.scan.basePackages}")
	public void setBasePackages(String basePackage) {
		this.basePackages = basePackage.split(",");
	}
}
