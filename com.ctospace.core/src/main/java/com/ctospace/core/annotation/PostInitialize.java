package com.ctospace.core.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * after AnnotationContext initialized, some times you want to get some
 * annotation from the context to do something, you can use this annotation on
 * the class, also should extends the {@link AbstractPostInitializeProcessor} or
 * implements the {@link PostInitializeProcessor}
 * */
@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface PostInitialize {

	/**
	 * annotation type, get the classes which annotated by this annotation type
	 * {@link AnnotationContext#getClasses(Class)}, then execute the
	 * {@link PostInitializeProcessor}
	 * */
	Class<Annotation> value();

}
