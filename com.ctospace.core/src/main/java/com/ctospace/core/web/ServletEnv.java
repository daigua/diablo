package com.ctospace.core.web;

import javax.servlet.ServletContext;

public class ServletEnv {
	
	private static ServletContext servletContext;
	
	public synchronized static void setServletContext(ServletContext servletContext){
		ServletEnv.servletContext = servletContext;
	}
	
	public static ServletContext getServletContext(){
		return ServletEnv.servletContext;
	}
	
	public static String getWebRoot(){
		return servletContext.getRealPath("/");
	}
}
