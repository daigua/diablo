package com.ctospace.core.web.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctospace.core.web.ServletEnv;

@WebListener
public class ServletEnvInitListener implements javax.servlet.ServletContextListener {
	
	private Logger logger = LoggerFactory.getLogger(ServletEnvInitListener.class);

	public void contextDestroyed(ServletContextEvent event) {
		ServletEnv.setServletContext(null);
	}

	public void contextInitialized(ServletContextEvent event) {
		logger.info("初始化ServletEnv");
		ServletEnv.setServletContext(event.getServletContext());
	}
}
