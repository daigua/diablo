package com.ctospace.core.classloader;

public interface XClassLoader {
	public Class<?> addClass(String className, byte[] classData);
}
