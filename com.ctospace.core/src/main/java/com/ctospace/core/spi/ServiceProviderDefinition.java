package com.ctospace.core.spi;

/**
 * T is the provider type
 * */
public class ServiceProviderDefinition {
	
	/**
	 * service provider's type
	 * */
	private Class<?> providerType;
	
	/**
	 * service's type 
	 * */
	private Class<?> serviceType;
	
	private int priority;
	
	public ServiceProviderDefinition(){
		this.priority = ProviderPriority.NORMAL;
	}
	
	public ServiceProviderDefinition(Class<?> serviceType, Class<?> providerType, int priority){
		this.providerType = providerType;
		this.serviceType = serviceType;
		this.priority = priority;
	}
	
	public ServiceProviderDefinition(Class<?> providerType, Class<?> serviceType){
		this(providerType, serviceType, ProviderPriority.NORMAL);
	}

	public Class<?> getProviderType() {
		return providerType;
	}

	public void setProviderType(Class<?> providerType) {
		this.providerType = providerType;
	}

	public Class<?> getServiceType() {
		return serviceType;
	}

	public void setServiceType(Class<?> serviceType) {
		this.serviceType = serviceType;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	
}
