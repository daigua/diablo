package com.ctospace.core.spi.bean;

public interface BeanContext {
	
	public <T> T getBean(String key);
	public <T> T getBean(Class<T> clazz);
	
}
