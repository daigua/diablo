package com.ctospace.core.spi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.ctospace.core.annotation.AnnotationContext;
import com.ctospace.core.annotation.AnnotationDefinition;
import com.ctospace.core.spi.annotation.ServiceProvider;

/**
 * load the service provider definition from annotation context, 
 * find classes announced by {@link ServiceProvider} 
 * */
public class AnnotationServiceProviderDefinitionLoader implements
		ServiceProviderDefinitionLoader {
	
	private AnnotationContext annotationContext;

	public AnnotationServiceProviderDefinitionLoader(
			AnnotationContext annotationContext) {
		this.annotationContext = annotationContext;
	}

	@Override
	public List<ServiceProviderDefinition> load() {

		Collection<AnnotationDefinition> annotationDefinitions = this.annotationContext
				.getAnnotationDefinitions(ServiceProvider.class);
		List<ServiceProviderDefinition> defs = new ArrayList<ServiceProviderDefinition>();
		for (AnnotationDefinition ad : annotationDefinitions) {
			ServiceProvider annotation = ad.getAnnotation();
			ServiceProviderDefinition def = new ServiceProviderDefinition(annotation.value(), ad.getTargetClass(),
					annotation.priority());
			defs.add(def);
		}
		return defs;
	}

}
