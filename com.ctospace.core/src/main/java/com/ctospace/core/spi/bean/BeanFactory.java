package com.ctospace.core.spi.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ctospace.core.bean.annotation.Bean;
import com.ctospace.core.spi.ServiceLoader;
import com.ctospace.core.spi.ServiceProviderDefinition;
import com.ctospace.core.spi.bean.BeanContext;
import com.ctospace.core.util.ClassUtils;

/**
 * the factory of the bean
 * */
@Component("com.ctospace.core.spi.bean.BeanFactory")
public class BeanFactory {

	private List<BeanContext> beanContexts;

	@Resource(name="com.ctospace.core.spi.ServiceLoader")
	private ServiceLoader serviceLoader;

	public BeanFactory() {
		beanContexts = new ArrayList<BeanContext>();
	}

	public void addBeanContext(BeanContext beanContext) {
		beanContexts.add(beanContext);
	}

	public void setBeanContext(List<BeanContext> beanContexts) {
		this.beanContexts = beanContexts;
	}

	/**
	 * init the beanfactory, it will find the BeanContext provider's in
	 * {@link ServiceLoader}, and the definition in file in classpath, default
	 * filename beanfactory.xml
	 * */
	@PostConstruct
	public void init() {
		//load BeanFactory's providers from ServiceLoader
		loadBeanContextProviders();
	}

	/**
	 * get the beancontext provider from serviceloader
	 * */
	private void loadBeanContextProviders() {
		List<ServiceProviderDefinition> beanContextProviderDefinitions = serviceLoader
				.load(BeanContext.class);
		for(ServiceProviderDefinition def : beanContextProviderDefinitions){
			BeanContext beanContext = (BeanContext) ClassUtils.newInstance(def.getProviderType());
			this.beanContexts.add(beanContext);
		}
	}

	/**
	 * get the bean from bean context by the string key, if can't find the bean
	 * ,then return null
	 * */
	public <T> T getBean(String key) {
		for (BeanContext beanContext : beanContexts) {
			T bean = beanContext.getBean(key);
			if (bean != null)
				return bean;
		}
		return null;
	}

	/**
	 * get bean from bean context by the clazz type, it will process the
	 * clazz's bean annotation, use the bean annotation value to find when the
	 * bean annotation exist and {@link Bean#value()} is not empty, if
	 * {@link Bean#value()} is empty, then use the clazz type to find, if bean
	 * annotation is not exist ,use clazz's type to find
	 * */
	public <T> T getBean(Class<T> clazz) {
		Bean beanAnnotation = clazz.getAnnotation(Bean.class);
		if (beanAnnotation == null) {
			for (BeanContext beanContext : beanContexts) {
				T bean = beanContext.getBean(clazz);
				if (bean != null)
					return bean;
			}
		} else {
			for (BeanContext beanContext : beanContexts) {
				String key = beanAnnotation.value();
				T bean;
				if (key.length() == 0)
					bean = beanContext.getBean(clazz);
				else
					bean = beanContext.getBean(key);
				if (bean != null)
					return bean;
			}
		}

		return null;
	}

	/**
	 * return the bean finded by the clazz in the bean context,
	 * 
	 * @param clazz
	 *            type of the bean
	 * @param strategy
	 *            when not find the bean, {@link NullStrategy#NULL} will return
	 *            null, {@link NullStrategy#NEW_INSTANCE} will return a new
	 *            instance created by clazz.newInstance()
	 * 
	 * */
	public <T> T getBean(Class<? extends T> clazz, NullStrategy strategy) {
		T bean = getBean(clazz);
		if (bean != null)
			return bean;
		if (strategy == NullStrategy.NEW_INSTANCE) {
			try {
				return clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}

		return null;
	}

	public void setBeanContexts(List<BeanContext> beanContexts) {
		this.beanContexts = beanContexts;
	}

	public void setServiceLoader(ServiceLoader serviceLoader) {
		this.serviceLoader = serviceLoader;
	}
}
