package com.ctospace.core.spi;

import java.util.List;

public interface ServiceProviderDefinitionLoader {
	public List<ServiceProviderDefinition> load();
}
