package com.ctospace.core.spi.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ctospace.core.spi.ProviderPriority;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ServiceProvider {

	/**
	 * 实现的provider的类型
	 * */
	Class<?> value();
	
	/**
	 * provider的优先级<br />
	 * default value {@link #ProviderPriority}<br />
	 * more see {@link #ProviderPriority}<br />
	 * */
	int priority() default ProviderPriority.NORMAL;
	
}
