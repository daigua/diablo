package com.ctospace.core.spi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ctospace.core.annotation.AnnotationContext;
import com.ctospace.core.spi.annotation.ServiceProvider;

@Component("com.ctospace.core.spi.ServiceLoader")
public class ServiceLoader {

	private Map<Class<?>, List<ServiceProviderDefinition>> cache;

	/**
	 * 用于保存spContext中某个key的value是否更改过，如果更改过则加标志
	 * */
	private Set<Class<?>> orderDirtyFlag;
	
	@Resource(name = "com.ctospace.core.annotation.AnnotationContext")
	private AnnotationContext annotationContext;

	public ServiceLoader() {
		cache = new HashMap<Class<?>, List<ServiceProviderDefinition>>();
		orderDirtyFlag = new HashSet<Class<?>>();
	}

	@PostConstruct
	public void init() {
		List<ServiceProviderDefinition> spds = new ArrayList<ServiceProviderDefinition>();
		
		AnnotationServiceProviderDefinitionLoader annotaitonLoader = new AnnotationServiceProviderDefinitionLoader(annotationContext);
		spds.addAll(annotaitonLoader.load());
		for(ServiceProviderDefinition def : spds){
			List<ServiceProviderDefinition> defs = cache.get(def.getServiceType());
			if(defs == null){
				defs = new ArrayList<ServiceProviderDefinition>();
				cache.put(def.getServiceType(), defs);
			}
			defs.add(def);
		}
		/**
		 * recently ,first not support load defintions from xml file. maybe support in future
		 * TODO support load defintions from xml files
		 * */
	}

	/**
	 * 注册ServiceProvider实例
	 * 
	 * @param clazz
	 *            provider的类型
	 * @param instance
	 *            该provider的实例
	 * */
	public void register(Class<?> clazz, ServiceProviderDefinition def) {
		List<ServiceProviderDefinition> providerTypes = cache.get(clazz);
		if (providerTypes == null) {
			providerTypes = new ArrayList<ServiceProviderDefinition>();
			cache.put(clazz, providerTypes);
		}
		/**
		 * 置标志位
		 * */
		orderDirtyFlag.add(clazz);
		providerTypes.add(def);
	}

	/**
	 * 返回Provider的提供者的class
	 * */
	public List<ServiceProviderDefinition> load(Class<?> clazz) {
		if (!cache.containsKey(clazz))
			return new ArrayList<ServiceProviderDefinition>();
		order(clazz);
		return cache.get(clazz);
	}

	/**
	 * order the clazz's service provider by {@link ServiceProvider#priority}
	 * desc
	 * */
	public void order(Class<?> clazz) {
		if (!orderDirtyFlag.contains(clazz)) {
			return;
		}
		List<ServiceProviderDefinition> providerTypes = cache.get(clazz);
		Collections.sort(providerTypes,
				new Comparator<ServiceProviderDefinition>() {
					@Override
					public int compare(ServiceProviderDefinition sp1,
							ServiceProviderDefinition sp2) {
						return sp2.getPriority() - sp1.getPriority();
					}
				});
		orderDirtyFlag.remove(clazz);
	}
}
