package com.ctospace.core.spi;

/**
 * 默认提供3种优先级
 * {@link #NORMAL}
 * {@link #HIGH}
 * {@link #LOW}
 * */
public class ProviderPriority {
	
	/**
	 * 默认优先级，默认值500
	 * */
	public final static int NORMAL = 500;
	/**
	 * 高优先级，值为1000
	 * */
	public final static int HIGH = 1000;
	/**
	 * 低优先级，值为0
	 * */
	public final static int LOW = 0;
	
}
