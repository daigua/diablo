package com.ctospace.core.spi.bean;

/**
 * 查找bean后的null值处理策略
 * */
public enum NullStrategy {
	/**
	 * 如果查找到bean为null，则通过newInstance()实例化一个
	 * */
	NEW_INSTANCE,
	/**
	 * 直接返回
	 * */
	NULL
}
