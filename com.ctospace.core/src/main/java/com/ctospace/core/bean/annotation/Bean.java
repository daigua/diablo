package com.ctospace.core.bean.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * annoter the class is a bean type, then find the instance from the BeanFactory
 * */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Bean {

	/**
	 * bean's id or name in the bean context
	 * */
	String value() default "";

}
