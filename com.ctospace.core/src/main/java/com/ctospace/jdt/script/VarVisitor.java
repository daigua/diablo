package com.ctospace.jdt.script;

import japa.parser.ast.body.VariableDeclarator;
import japa.parser.ast.expr.VariableDeclarationExpr;
import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.ExpressionStmt;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.util.ArrayList;
import java.util.List;

public class VarVisitor extends VoidVisitorAdapter<Object> {
	
	private List<VarDef> varDefs;
	
	public VarVisitor(){
		this.varDefs = new ArrayList<VarDef>();
	}
	
	public List<VarDef> getVarDefs(){
		return this.varDefs;
	}
	
	@Override
	public void visit(BlockStmt n, Object arg) {
		if (n.getStmts() != null) {
            for (Statement s : n.getStmts()) {
            	if(s instanceof ExpressionStmt)
            		s.accept(this, arg);
            }
        }
	}

	public void visit(VariableDeclarationExpr n, Object arg) {
		String type = n.getType().toString();
        for (VariableDeclarator v : n.getVars()) {
            VarDef def = new VarDef(type, v.getId().toString());
            this.varDefs.add(def);
        }
    }

}