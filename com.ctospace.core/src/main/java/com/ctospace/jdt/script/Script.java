package com.ctospace.jdt.script;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.ImportDeclaration;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.visitor.VoidVisitor;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.tools.JavaFileObject;

import com.ctospace.core.classloader.XClassLoader;
import com.ctospace.jdt.compiler.CompileException;
import com.ctospace.jdt.compiler.CompileHelper;
import com.ctospace.jdt.compiler.JavaClass;
import com.ctospace.jdt.compiler.JavaSource;

public class Script {

	private Map<String, Object> varContext;
	private List<VarDef> varDefs;
	private Set<String> imports;
	private XClassLoader classLoader;
	private List<String> methods;
	private static final Set<String> ALLWAYS_IMPORTS = new HashSet<String>();
	static{
		ALLWAYS_IMPORTS.add("import " + Script.class.getName() + ";");
	}
	
	private List<VarDef> willAddVarDefs;
	private List<String> willAddMethods;
	
	private InputHandleFilterChain inputHandleFilterChain;
	
	private static Map<String,String> WRAP_TYPE_MAPPING = new HashMap<String,String>();
	static {
		WRAP_TYPE_MAPPING.put("int", "Integer");
		WRAP_TYPE_MAPPING.put("long", "Long");
		WRAP_TYPE_MAPPING.put("double", "Double");
		WRAP_TYPE_MAPPING.put("float", "Float");
	}
	
	private static String template = "package {package};\n" + "{imports}\n"
							+ "public class {className}{\n"
							+ "	public void run(Script script){\n"
							+ "		this.runProxy(script{callArgs});\n" + "	}\n"
							+ "	private void runProxy(Script script{args}){\n" + "		" +
									"{body}\n"
							+ "		{saveArgs}" + ""
							+ "	}\n" +
									"{methods}" 
							+ "}\n";

	private static String INPUT_WRAP = "public class Wrap{\n" +
							"		public void wrapMethod(){\n" +
							"			{body}" +
							"		}\n" +
							"  }";
	
	private int id = 0;
	
	private File classFolder;
	
	public Script(XClassLoader classLoader){
		this(classLoader, null);
	}

	public Script(XClassLoader classLoader, String classFolderPath) {
		varContext = new HashMap<String, Object>();
		imports = new HashSet<String>();
		varDefs = new ArrayList<VarDef>();
		this.classLoader = classLoader;
		this.methods = new ArrayList<String>();
		initInputHandleFilterChain();
		if(classFolderPath == null){
			//为当前类的目录
			classFolderPath = this.getClass().getClassLoader().getResource("").getFile();
		}
		classFolder = new File(classFolderPath);
		if(!classFolder.exists())
			classFolder.mkdirs();
	}
	
	private void initInputHandleFilterChain(){
		inputHandleFilterChain = new InputHandleFilterChain();
		ClearHandleFilter clearFilter = new ClearHandleFilter();
		inputHandleFilterChain.addFilter(clearFilter);
		
		ClassHandleFilter classFilter = new ClassHandleFilter();
		inputHandleFilterChain.addFilter(classFilter);
		
		MethodHandleFilter methodFilter = new MethodHandleFilter();
		inputHandleFilterChain.addFilter(methodFilter);
		
		ExpressionHandleFilter expressionFilter = new ExpressionHandleFilter();
		inputHandleFilterChain.addFilter(expressionFilter);
		
	}
	
	public void addImport(String importStr){
		importStr = importStr.trim().replaceAll("\\s{2,}", " ");
		this.imports.add(importStr);
	}
	
	public Object getVarValue(String varName) {
		return this.varContext.get(varName);
	}

	public void saveArgValue(String varName, Object value) {
		this.varContext.put(varName, value);
	}

	public void execute(String input) throws CompileException {
		input = input.trim();
		this.willAddVarDefs = new ArrayList<VarDef>();
		this.willAddMethods = new ArrayList<String>();
		this.inputHandleFilterChain.doChain(input, this);
		this.inputHandleFilterChain.resetIndex();
		for(VarDef vd : this.willAddVarDefs){
			this.varDefs.add(vd);
		}
		for(String method : this.willAddMethods){
			this.methods.add(method);
		}
	}
	
	protected void runExpressionStmt(String input){
		/**
		 * 为了对付这样的情况
		 * 一次性输入
		 * import java.util.Date; Date date = new Date();的情况。
		 * 这种情况下 import 必须先声明。
		 * */
		int index = 0;
		if(input.startsWith("import ")){
			String[] units = input.split(";");
			for(String unit : units){
				if(unit.startsWith("import ")){
					this.addImport(unit+";");
					index += unit.length()+1;
				}else{
					break;
				}
			}
		}
		input = input.substring(index);
		String className = "com.ctospace.jdt.script.TestR" + (id++);
		String source = null;
		try {
			source = this.buildJava(className, input);
		} catch (ParseException e1) {
			throw new CompileException(e1.getMessage());
		}
		Class<?> clazz = this.compileJavaSource(className, source);
		Object runner;
		try {
			runner = clazz.newInstance();
			Method method = clazz.getMethod("run", Script.class);
			method.invoke(runner, this);
		} catch (Exception e) {
			
		} 
		
	}
	
	private Class<?> compileJavaSource(String className, String javaSource){
		return this.compileJavaSource(className, javaSource, false);
	}
	
	private Class<?> compileJavaSource(String className, String javaSource, boolean saveClassFile) throws CompileException{
		JavaClass clazzFileObject = tryCompileJavaSource(className, javaSource);
		Class<?> clazz = classLoader.addClass(className,
				clazzFileObject.getBytes());
		if(saveClassFile){
			String classFileFolder = className.substring(0, className.lastIndexOf(".")).replace(".", "/");
			File folderFile = new File(this.classFolder + "/" + classFileFolder);
			if(!folderFile.exists())
				folderFile.mkdirs();
			String filename = className.substring(className.lastIndexOf(".") + 1, className.length())+".class";
			File file = new File(folderFile, filename);
			try {
				if(!file.exists())
					file.delete();
				file.createNewFile();
				OutputStream os = new FileOutputStream(file);
				os.write(clazzFileObject.getBytes());
				os.flush();
				os.close();
			} catch (Exception e) {
				throw new RuntimeException(e);
			} 
		}
		return clazz;
		
	}
	
	private JavaClass tryCompileJavaSource(String className, String source) throws CompileException{
		System.out.println(source);
		URI uri = URI.create("file:///" + className.replace(".", "/")
				+ JavaFileObject.Kind.SOURCE.extension);
		JavaSource javaSource = new JavaSource(className, uri, source,
				Charset.forName("utf-8"));
		CompileHelper compilier = CompileHelper.getInstance();
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PrintWriter out = new PrintWriter(bos);
		Iterable<String> options = Arrays.asList("-classpath", this.classFolder.getAbsolutePath());
		JavaClass clazzFileObject = compilier.compile(javaSource, options, out);
		return clazzFileObject;
	}

	private String buildJava(String className, String body) throws ParseException {
		StringBuilder argsBuilder = new StringBuilder();
		StringBuilder callArgsBuilder = new StringBuilder();
		StringBuilder saveArgsBuilder = new StringBuilder();
		for (VarDef vd : this.varDefs) {
			callArgsBuilder.append(",("+toWrapType(vd.getType())+")script.getVarValue(\"" + vd.getName()
					+ "\")");
			argsBuilder.append("," + vd.getType() + " " + vd.getName());
		}
		String classScript = template;
		String pkg = className.substring(0, className.lastIndexOf("."));
		className = className.substring(className.lastIndexOf(".") + 1);
		List<VarDef> vars = parseVarDefs(body);
		for (VarDef vd : vars) {
			if (!this.varDefs.contains(vd)) {
				willAddVarDefs.add(vd);
			}
			saveArgsBuilder.append("script.saveArgValue(\"" + vd.getName()
					+ "\", " + vd.getName() + ");\n");
		}
		
		classScript = classScript.replace("{package}", pkg);
		classScript = classScript.replace("{imports}", getImportsSource());
		classScript = classScript.replace("{className}", className);
		classScript = classScript.replace("{body}", body);
		classScript = classScript.replace("{args}", argsBuilder.toString());
		classScript = classScript.replace("{callArgs}",	callArgsBuilder.toString());
		classScript = classScript.replace("{saveArgs}",	saveArgsBuilder.toString());
		classScript = classScript.replace("{methods}", getMethodSource());
		return classScript;
	}
	
	private String getImportsSource(){
		StringBuilder impBuilder = new StringBuilder();
		for (String imp : this.imports) {
			impBuilder.append(imp + "\n");
		}
		for(String imp : ALLWAYS_IMPORTS){
			impBuilder.append(imp + "\n");
		}
		return impBuilder.toString();
	}
	
	private String getMethodSource(){
		StringBuilder methodsBuilder = new StringBuilder();
		for(String method : this.methods){
			methodsBuilder.append(method + "\n");
		}
		return methodsBuilder.toString();
	}
	
	public static final String METHOD_TEST_CLASSNAME = "TestMethod";
	public static final String METHOD_TEST_TEMPLATE = "{imports}\npublic class "+METHOD_TEST_CLASSNAME+"{ {methods} }";
	
	public void tryCompileMethod(String input){
		String source = METHOD_TEST_TEMPLATE.replace("{imports}", this.getImportsSource());
		source = source.replace("{methods}", input + "\n" + this.getMethodSource());
		this.tryCompileJavaSource(METHOD_TEST_CLASSNAME, source);
	}
	public static final String IMPORT_TEST_CLASSNAME = "TestImport";
	public static final String IMPORT_TEST_TEMPLATE = "\npublic class "+IMPORT_TEST_CLASSNAME+"{}";
	
	public void tryCompileImport(String imports){
		imports += IMPORT_TEST_TEMPLATE;
		this.tryCompileJavaSource(IMPORT_TEST_CLASSNAME, imports);
	}
	
	public void addMethod(String method){
		this.methods.add(method);
	}
	
	private String toWrapType(String type){
		type = type.trim();
		String wrapType = WRAP_TYPE_MAPPING.get(type);
		return wrapType == null ? type : wrapType;
	}
	
	private List<VarDef> parseVarDefs(String input) throws ParseException {
		String classStr = INPUT_WRAP.replace("{body}", input);
		CompilationUnit cu = JavaParser.parse(new ByteArrayInputStream(classStr.getBytes()));
		VarVisitor visitor = new VarVisitor();
		visitor.visit(cu, null);
		return visitor.getVarDefs();
	}
	
	private void clear() {
		imports.clear();
		this.varDefs.clear();
		varContext = new HashMap<String, Object>();
	}
	
	/**
	 * clear input handler filter
	 * */
	private static class ClearHandleFilter implements InputHandleFilter{

		@Override
		public void handle(String input, Script script, InputHandleFilterChain chain) {
			if(!input.equals("clear")){
				chain.doChain(input, script);
			}else{
				script.clear();
			}
		}
		
	}
	
	private static class ClassHandleFilter implements InputHandleFilter{
		
		@Override
		public void handle(String input, Script script,
				InputHandleFilterChain chain) {
			try {
				CompilationUnit cu = JavaParser.parse(new ByteArrayInputStream(input.getBytes()));
				final StringBuilder className = new StringBuilder();
				VoidVisitor<Object> visitor = new VoidVisitorAdapter<Object>(){
					@Override
					public void visit(ClassOrInterfaceDeclaration n, Object arg) {
						if(n.getModifiers() == 1)
							className.append(n.getName());
						else
							super.visit(n, arg);
					}
					
				};
				cu.accept(visitor, null);
				String cname = className.toString();
				/***
				 * 如果class名称不存在，则说明input 只是输入了 import 而没有输入class主体 如：
				 * import com.ctospace.script.*;
				 */
				if(cname.isEmpty()){
					List<ImportDeclaration> imports = cu.getImports();
					script.tryCompileImport(input);
					if(imports.size() > 0){
						for(ImportDeclaration importD : imports){
							script.addImport(importD.toString());
						}
					}
				}else{
					script.compileJavaSource(cu.getPackage().getName() + "." + className, input, true);
				}
			} catch (ParseException e) {
				chain.doChain(input, script);
			}
		}
	}
	
	private static class MethodHandleFilter implements InputHandleFilter{
		
		public static final String MODIFIERS = "public|private|static|final";

		@Override
		public void handle(String input, Script script,
				InputHandleFilterChain chain) {
			int dnaIndex = input.indexOf("(");
			if(dnaIndex == -1){
				chain.doChain(input, script);
				return;
			}
			String methodDna = input.substring(0, dnaIndex);
			/**
			 * 排除掉     x = y(); x += y(); 的情况
			 * */
			String[] dnas = methodDna.split("\\s");
			if(dnas.length > 4 || dnas.length == 1){
				chain.doChain(input, script);
			}
			
			for(String dna : dnas){
				if(dna.matches(MODIFIERS)){
					script.tryCompileMethod(input);
					script.addMethod(input);
				}
			}
			
		}
		
	}
	
	private static class ExpressionHandleFilter implements InputHandleFilter{
		@Override
		public void handle(String input, Script script, InputHandleFilterChain chain) {
			script.runExpressionStmt(input);
		}
	}
	
	private static class InputHandleFilterChain{
		private List<InputHandleFilter> filters;
		
		private int index;
		
		private int filterSize;
		
		public InputHandleFilterChain(){
			this.filters = new ArrayList<InputHandleFilter>();
			index = 0;
		}
		
		public void doChain(String input, Script script){
			if(filterSize > index){
				InputHandleFilter filter = this.filters.get(index++);
				filter.handle(input, script, this);
			}
		}
		
		public void addFilter(InputHandleFilter filter){
			this.filters.add(filter);
			this.filterSize ++;
		}
		
		public void resetIndex(){
			this.index = 0;
		}
		
	}
	
	private static interface InputHandleFilter{
		public void handle(String input, Script script, InputHandleFilterChain chain);
	}
	
}
