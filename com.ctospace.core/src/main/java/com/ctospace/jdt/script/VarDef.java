package com.ctospace.jdt.script;

public class VarDef{
	private String type;
	private String name;
	
	public VarDef(String type, String name){
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return this.type + " : " + this.name;
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof VarDef))
			return false;
		VarDef _obj = (VarDef) obj;
		return _obj.getName().equals(this.name);
	}
	
	
}
