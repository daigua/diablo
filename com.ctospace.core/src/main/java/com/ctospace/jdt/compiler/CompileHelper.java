package com.ctospace.jdt.compiler;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.tools.*;

public class CompileHelper {

	private static JavaCompiler javaCompiler;
	private StandardJavaFileManager standardJavaFileManager;
	public final static String DEFAULT_CHARSET = "utf-8";

	private DiagnosticListener<? super JavaFileObject> diagnosticListener;

	private CompileHelper() {
		javaCompiler = ToolProvider.getSystemJavaCompiler();
		diagnosticListener = new DiagnosticListener<JavaFileObject>() {

			public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
				StringBuilder sb = new StringBuilder();
				sb.append(diagnostic.getMessage(null) + "\n");
				System.out.println(sb);
			}
		};
		this.standardJavaFileManager = javaCompiler.getStandardFileManager(
				diagnosticListener, null, null);
	}

	private static class CompileHelperHolder {
		static CompileHelper compiler = new CompileHelper();
	}

	public static CompileHelper getInstance() {
		return CompileHelperHolder.compiler;
	}

	public Map<String, JavaClass> compile(List<JavaSource> javaSources,
			Iterable<String> options, PrintWriter out) throws CompileException {
		ByteJavaFileManager scriptJavaFileManager = new ByteJavaFileManager(
				standardJavaFileManager);
		JavaCompiler.CompilationTask compile = javaCompiler.getTask(out,
				scriptJavaFileManager, diagnosticListener, options, null,
				javaSources);
		if (!compile.call()) {
			throw new CompileException("Compile error");
		}
		return scriptJavaFileManager.getScriptClassMapping();
	}

	public JavaClass compile(JavaSource scriptJava, Iterable<String> options,
			PrintWriter out) throws CompileException {
		Map<String, JavaClass> scriptClassMapping = this.compile(
				Arrays.asList(scriptJava), options, out);
		return scriptClassMapping.get(scriptJava.getClassName());
	}

}
