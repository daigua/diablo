package com.ctospace.jdt.compiler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.Charset;

import javax.tools.*;

public class JavaSource extends SimpleJavaFileObject {

	private String source;
	
	private String className;

	private Charset charset;
	
	public JavaSource(String className, String source, Charset charset){
		this(className, URI.create("file:///" + className.replace(".", "/")
				+ JavaFileObject.Kind.SOURCE.extension), source, charset);
	}

	public JavaSource(String className, URI uri, String source, Charset charset) {
		super(uri, JavaFileObject.Kind.SOURCE);
		this.source = source;
		this.charset = charset;
		this.className = className;
	}

	public CharSequence getCharContent(boolean ignoreEncodingErrors) {
		return source;
	}

	public OutputStream openOutputStream() {
		throw new IllegalStateException();
	}

	public InputStream openInputStream() {
		return new ByteArrayInputStream(
				charset == null ? source.getBytes(charset) : source.getBytes());
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Charset getCharset() {
		return charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	public String getClassName() {
		return className;
	}
}
