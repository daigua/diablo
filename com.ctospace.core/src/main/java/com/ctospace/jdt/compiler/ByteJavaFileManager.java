package com.ctospace.jdt.compiler;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.tools.*;
import javax.tools.JavaFileObject.Kind;

public class ByteJavaFileManager extends ForwardingJavaFileManager<JavaFileManager> {
	
	private Map<String,JavaClass> javaClassMapping;

	protected ByteJavaFileManager(JavaFileManager fileManager) {
		super(fileManager);
		javaClassMapping = new HashMap<String,JavaClass>();
	}

	@Override
	public JavaFileObject getJavaFileForOutput(Location location,
			String className, Kind kind, FileObject sibling) throws IOException {
		String classFilePath = sibling.getName();
		classFilePath = classFilePath.substring(1);
		String classFullName = classFilePath.substring(0, classFilePath.lastIndexOf(".")).replace("/", ".");
		URI uri = URI.create("byte:///" + classFullName + Kind.CLASS.extension);
		JavaClass javaClass = new JavaClass(uri);
		javaClassMapping.put(classFullName, javaClass);
		return javaClass;
	}
	
	public Map<String,JavaClass> getScriptClassMapping(){
		return this.javaClassMapping;
	}

	public JavaClass getScriptClass(String className){
		return this.javaClassMapping.get(className);
	}

}
