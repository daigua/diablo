package com.ctospace.spring.test;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring.xml")
public class SpringTest {
	
	@Resource
	private BeanFactory beanFactory;

	@Test
	public void test(){
		TestBean testBean = beanFactory.getBean(TestBean.class);
		Assert.assertNotNull(testBean);
	}
	
}
