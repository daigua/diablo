package com.ctospace.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.ctospace.core.spi.ProviderPriority;
import com.ctospace.core.spi.annotation.ServiceProvider;
import com.ctospace.core.spi.bean.BeanContext;

@Component("com.ctospace.spring.SpringBeanContext")
@ServiceProvider(value=BeanContext.class, priority=ProviderPriority.HIGH)
public class SpringBeanContext implements BeanContext, ApplicationContextAware {
	
	private static SpringBeanContextInner springBeanContextInner;
	
	@SuppressWarnings("unchecked")
	public <T> T getBean(String key) {
		return (T) springBeanContextInner.getBean(key);
	}

	public <T> T getBean(Class<T> clazz) {
		return springBeanContextInner.getBean(clazz);
	}

	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		if(springBeanContextInner != null){
			throw new RuntimeException("Duplicate applicationContext set");
		}
		springBeanContextInner = new SpringBeanContextInner(applicationContext);
	}
	
	private static class SpringBeanContextInner implements BeanContext{
		private ApplicationContext applicationContext;
		
		public SpringBeanContextInner(ApplicationContext applicationContext){
			this.applicationContext = applicationContext;
		}

		@SuppressWarnings("unchecked")
		public <T> T getBean(String key) {
			try {
				return (T) applicationContext.getBean(key);
			} catch (NoSuchBeanDefinitionException e) {
				return null;
			}
		}

		public <T> T getBean(Class<T> clazz) {
			try {
				return (T) applicationContext.getBean(clazz);
			} catch (NoSuchBeanDefinitionException e) {
				return null;
			}
		}
		
	}

}
