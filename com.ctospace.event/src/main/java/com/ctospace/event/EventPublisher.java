package com.ctospace.event;

import java.util.List;

public interface EventPublisher {

	public <T extends Event, R extends EventListener<T>> void publish(T event, List<R> listeners);
	
}
