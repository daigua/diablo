package com.ctospace.event;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import com.ctospace.core.spi.bean.BeanFactory;
import com.ctospace.core.util.AnnotationUtils;
import com.ctospace.core.util.ClassUtils;
import com.ctospace.core.util.reflect.MethodUtils;
import com.ctospace.event.annotation.EventFeedback;
import com.ctospace.event.annotation.EventParam;
import com.ctospace.event.annotation.Listen;

public class AnnotationEventListener<T extends Event> implements
		EventListener<T> {

	private Class<?> targetClass;
	private Method method;
	private BeanFactory beanFactory;
	private Listen listenAnnotation;
	private ListenerInvoke<T> listenerInvoke;
	private Class<?> eventType;

	public AnnotationEventListener(Class<?> targetClass, Method method,
			BeanFactory beanFactory) {
		this.targetClass = targetClass;
		this.method = method;
		this.beanFactory = beanFactory;
		this.listenAnnotation = method.getAnnotation(Listen.class);
		this.listenerInvoke = this.createListenerInvoke();
	}

	@Override
	public void handle(T event) {
		Object target = beanFactory.getBean(targetClass);
		listenerInvoke.invoke(target, event);
	}

	private ListenerInvoke<T> createListenerInvoke() {
		ListenerInvoke<T> invoke = null;
		eventType = this.listenAnnotation.eventType();
		final Class<?>[] parameters = method.getParameterTypes();
		boolean _isFeedback = false;
		if (method.getAnnotation(EventFeedback.class) != null) {
			if (!ClassUtils.isAssignable(eventType, FeedbackAble.class)) {
				throw new RuntimeException(
						"A method announced by a @EventFeedback must listen a feedback able event");
			}
			_isFeedback = true;
		}
		final boolean isFeedback = _isFeedback;
		if (eventType == NullEvent.class) {
			/**
			 * here, method must have one parameter and the type of this
			 * parameter must implements Event
			 * */
			if (parameters.length != 1
					|| !ClassUtils.isAssignable(parameters[0],Event.class)) {
				throw new RuntimeException(
						"Must have one parameter or the parameter's type must implement Event");
			}
			eventType = parameters[0];
			invoke = new ListenerInvoke<T>() {

				@Override
				public void invoke(Object target, T event) {
					Object result = MethodUtils.invoke(method, target, event);
					handleInvokeReturn(event, isFeedback, result);
				}

			};
		} else {
			/**
			 * invoke method with no parameter
			 * */
			if (parameters.length == 0) {
				invoke = new ListenerInvoke<T>() {

					@Override
					public void invoke(Object target, T event) {
						Object result = MethodUtils.invoke(method, target);
						handleInvokeReturn(event, isFeedback, result);
					}

				};
			} else if (parameters.length == 1 && parameters[0] == eventType) {
				invoke = new ListenerInvoke<T>() {
					@Override
					public void invoke(Object target, T event) {
						Object result = MethodUtils.invoke(method, target,
								event);
						handleInvokeReturn(event, isFeedback, result);
					}

				};
			} else {
				final String[] paramNames = getParamNames(method);
				final Method[] paramMethods = new Method[paramNames.length];
				int methodI = 0;
				/**
				 * 根据参数名的getter方法取得Event的Method
				 * */
				for (String paramName : paramNames) {
					paramMethods[methodI++] = MethodUtils.getGetterMethod(
							eventType, paramName);
				}
				invoke = new ListenerInvoke<T>() {

					@Override
					public void invoke(Object target, T event) {
						Object[] params = new Object[paramNames.length];
						int i = 0;
						for (Method method : paramMethods) {
							params[i++] = MethodUtils.invoke(method, event);
						}
						Object result = MethodUtils.invoke(method, target,
								params);
						handleInvokeReturn(event, isFeedback, result);
					}

				};
			}
		}

		return invoke;
	}

	/**
	 * handle event feedback
	 * */
	private void handleInvokeReturn(T event, boolean isFeedback,
			Object returnValue) {
		if(!isFeedback)
			return;
		Feedback<Object> feedback = new Feedback<>(); 
		feedback.setResult(returnValue);
		@SuppressWarnings("unchecked")
		FeedbackAble<T,Object> feedbackAble = (FeedbackAble<T,Object>) event;
		feedbackAble.add(feedback);
	}

	private String[] getParamNames(Method method) {
		Annotation[][] paramsAns = method.getParameterAnnotations();
		String[] names = new String[paramsAns.length];
		String[] methodParamNames = MethodUtils.getMethodParameterNames(method);
		for (int i = 0; i < paramsAns.length; i++) {
			Annotation[] paramAns = paramsAns[i];
			EventParam eventParam = AnnotationUtils.getAnnotation(paramAns,
					EventParam.class);
			if (eventParam == null || eventParam.value().length() == 0) {
				// get the parameter name ,otherwise throw exception
				names[i] = methodParamNames[i];
			} else {
				names[i] = eventParam.value();
			}
		}
		return names;
	}

	public static interface ListenerInvoke<T> {
		public void invoke(Object target, T event);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<T> eventType() {
		return (Class<T>) this.eventType;
	}

}
