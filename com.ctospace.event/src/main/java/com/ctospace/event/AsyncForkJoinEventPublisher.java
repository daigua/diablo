package com.ctospace.event;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 * Fork-Join模式异步事件发布器, 
 * 当该事件的listener操作比较耗时的时候建议采用该模式的发布器
 * */
public class AsyncForkJoinEventPublisher implements EventPublisher {

	private ForkJoinPool forkJoinPool;
	
	/**
	 * 使用该构造器，需要通过{@link #setForkJoinPool(ForkJoinPool)}显示注册一个ForkJoinPool
	 * */
	public AsyncForkJoinEventPublisher() {
		
	}
	
	public AsyncForkJoinEventPublisher(ForkJoinPool forkJoinPool){
		this.forkJoinPool = forkJoinPool;
	}

	@Override
	public <T extends Event, R extends EventListener<T>> void publish(T event,
			List<R> listeners) {
		if (listeners.size() == 0)
			return;
		for(R listener : listeners){
			ForkJoinPublishTask<T,R> task = new ForkJoinPublishTask<T,R>(event, listener);
			forkJoinPool.submit(task);
			task.join();
		}
	}

	public static class ForkJoinPublishTask<T extends Event,R extends EventListener<T> > extends RecursiveAction{
		
		private static final long serialVersionUID = 1L;
		
		private R listener;
		
		private T event;
		
		public ForkJoinPublishTask(T event, R listener){
			this.listener = listener;
			this.event = event;
		}

		@Override
		protected void compute() {
			listener.handle(this.event);
		}
		
	}

	public void setForkJoinPool(ForkJoinPool forkJoinPool) {
		this.forkJoinPool = forkJoinPool;
	}
}
