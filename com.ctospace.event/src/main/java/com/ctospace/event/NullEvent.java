package com.ctospace.event;

public class NullEvent extends Event {

	private static final long serialVersionUID = 1L;

	public NullEvent(Object source) {
		super(source);
		throw new RuntimeException(
				"This class is only for the default value of {@link Listen#eventType}");
	}

}
