package com.ctospace.event;

/**
 * feedback of the listener handle event
 * E event type
 * T feedback type
 * */
public class Feedback<T> {
	
	private T result;
	private String message;
	
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
