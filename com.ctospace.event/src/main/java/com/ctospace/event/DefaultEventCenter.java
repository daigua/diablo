package com.ctospace.event;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ctospace.core.util.ClassUtils;

/**
 * default event center implement
 * */
@Component("com.ctospace.event.DefaultEventCenter")
public class DefaultEventCenter implements EventCenter {

	@Resource(name = "com.ctospace.event.EventPublisherFactory")
	private EventPublisherFactory eventPublisherFactory;

	@Resource(name = "com.ctospace.event.EventContext")
	private EventContext eventContext;

	@Override
	public <T extends Event> void publish(T event) {
		this.publish(event, Mode.SYNC);
	}

	@Override
	public <T extends Event> void publish(T event, Mode mode) {
		if (ClassUtils.isAssignable(event.getClass(), FeedbackAble.class)
				&& mode == Mode.ASYNC) {
			throw new RuntimeException(
					"can not publish FeedbackAble event by Mode.ASYNC mode, should use Mode.SYNC or Mode.ASYNC_FORK_JOIN.");
		}
		List<EventListener<T>> listeners = this.eventContext
				.getEventListeners(event.getClass());
		EventPublisher eventPublisher = eventPublisherFactory
				.getEventPublisher(mode);
		eventPublisher.publish(event, listeners);
	}

	public void setEventPublisherFactory(
			EventPublisherFactory eventPublisherFactory) {
		this.eventPublisherFactory = eventPublisherFactory;
	}

}
