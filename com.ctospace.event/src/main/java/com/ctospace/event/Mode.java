package com.ctospace.event;

/**
 * 事件处理模式，分为<br />
 * {@link #SYNC} 同步模式 <br />
 * {@link #ASYNC} 异步模式 <br />
 * {@link #AYSNC_FORK_JOIN} 异步fork-join模式
 * */
public enum Mode {
	/**
	 * 同步处理该事件，所有事件监听器都同步监听该事件
	 * */
	SYNC,
	/**
	 * 异步处理该事件
	 * */
	ASYNC,
	/**
	 * 以fork-join模式异步处理该事件，事件处理是异步的，但是同步返回
	 * */
	AYSNC_FORK_JOIN
	
}
