package com.ctospace.event;

import java.util.List;

/**
 * 以同步方式发布的事件中心
 * */
public class SyncEventPublisher implements EventPublisher {

	@Override
	public <T extends Event, R extends EventListener<T>> void publish(T event,
			List<R> listeners) {
		for(R listener : listeners){
			listener.handle(event);
		}
	}

	

	

}
