package com.ctospace.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ctospace.core.annotation.AnnotationContext;
import com.ctospace.core.annotation.AnnotationDefinition;
import com.ctospace.core.spi.bean.BeanFactory;
import com.ctospace.core.util.reflect.MethodUtils;
import com.ctospace.event.annotation.Listen;
import com.ctospace.event.annotation.Listener;

@Component("com.ctospace.event.AnnotationEventListenerLoader")
public class AnnotationEventListenerLoader implements EventListenerLoader {

	@Resource(name="com.ctospace.core.annotation.AnnotationContext")
	private AnnotationContext annotationContext;
	@Resource(name="com.ctospace.core.spi.bean.BeanFactory")
	private BeanFactory beanFactory;

	@Override
	public List<EventListener<?>> load() {

		Collection<AnnotationDefinition> annotationDefinitions = this.annotationContext
				.getAnnotationDefinitions(Listener.class);
		List<EventListener<?>> listeners = new ArrayList<EventListener<?>>();

		for (AnnotationDefinition def : annotationDefinitions) {
			listeners.addAll(createEventListener(def));
		}

		return listeners;
	}

	private List<EventListener<?>> createEventListener(
			AnnotationDefinition annotationDef) {
		List<EventListener<?>> listeners = new ArrayList<EventListener<?>>();
		Class<?> targetClass = annotationDef.getTargetClass();
		Method[] listenMethods = MethodUtils.getMethods(targetClass,
				Listen.class);
		if (listenMethods.length == 0)
			return listeners;
		for (Method method : listenMethods) {
			AnnotationEventListener<?> listener = new AnnotationEventListener<>(
					targetClass, method, beanFactory);
			listeners.add(listener);
		}
		return listeners;
	}

}
