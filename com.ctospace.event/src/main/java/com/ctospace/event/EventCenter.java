package com.ctospace.event;

/**
 * 事件中心
 * */
public interface EventCenter {

	/**
	 * 以同步的方式发布事件
	 * 
	 * @param event
	 *            发布的事件
	 * */
	public <T extends Event> void publish(T event);

	/**
	 * 以相应的模式发布事件
	 * @param event
	 * 发布的事件
	 * @param mode {@link #Mode}
	 * */
	public <T extends Event> void publish(T event, Mode mode);

}
