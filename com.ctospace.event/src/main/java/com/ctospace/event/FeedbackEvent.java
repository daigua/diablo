package com.ctospace.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FeedbackEvent<E extends Event, T> extends Event implements FeedbackAble<E, T> {

	private static final long serialVersionUID = 1L;
	private List<Feedback<T>> feedbacks;

	public FeedbackEvent(Object source) {
		super(source);
		feedbacks = new ArrayList<Feedback<T>>();
	}
	
	@Override
	public void add(Feedback<T> feedback) {
		feedbacks.add(feedback);
	}

	@Override
	public List<Feedback<T>> getFeedbacks() {
		return Collections.unmodifiableList(feedbacks);
	}

}
