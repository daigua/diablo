package com.ctospace.event;

import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ctospace.core.spi.bean.BeanFactory;
import com.ctospace.core.util.PropertiesUtils;

/**
 * the configuration of the event center, this is a singleton, and lazy init on
 * the first time by {@link #getInstance()}
 * */
@Component("com.ctospace.event.Configuration")
public class Configuration {

	@Autowired(required=false)
	@Qualifier("java.util.concurrent.ThreadPoolExecutor")
	private ThreadPoolExecutor threadPoolExecutor;
	
	private BeanFactory beanFactory;

	@Autowired(required=false)
	@Qualifier("java.util.concurrent.ForkJoinPool")
	private ForkJoinPool forkJoinPool;

	public final static String FORKJOINPOOL_BEAN_KEY = "eventcenter.forkjoinpool.bean";

	// default ThreadPoolExecutor param definition
	private final static String THREADPOOLEXECUTOR_PREFIX = "eventcenter.threadpoolexecutor.";
	// The default ThreadPoolExecutor's bean name which find from the
	// bean context
	public final static String THREADPOOLEXECUTOR_BEAN_KEY = THREADPOOLEXECUTOR_PREFIX
			+ "bean";
	// The ThreadPoolExecutor's default construct parameters
	public final static String THREADPOOLEXECUTOR_COREPOOLSIZE_KEY = THREADPOOLEXECUTOR_PREFIX
			+ "corepoolsize";
	public final static String THREADPOOLEXECUTOR_MAXPOOLSIZE_KEY = THREADPOOLEXECUTOR_PREFIX
			+ "maxpoolsize";
	public final static String THREADPOOLEXECUTOR_KEEPALIVETIME_KEY = THREADPOOLEXECUTOR_PREFIX
			+ "keepalivetime";
	public final static String THEADPOOLEXECUTOR_TIMEUNIT_KEY = THREADPOOLEXECUTOR_PREFIX
			+ "timeunit";
	public final static String THREADPOOLEXECUTOR_BLOCKQUEUE_TYPE_KEY = THREADPOOLEXECUTOR_PREFIX
			+ "blockqueue.type";

	public final static String DEFAULT_CONFIG_FILE = "com/ctospace/event/eventcenter.properties";

	public final static String CONFIG_FILE = "eventcenter.properties";

	private Properties properties;

	/**
	 * init the event configuration
	 * */
	@PostConstruct
	public void init() {
		Properties defaultProperties = PropertiesUtils
				.getFromClassPath(DEFAULT_CONFIG_FILE);
		Properties properties = PropertiesUtils.getFromClassPath(CONFIG_FILE);
		if (properties != null) {
			this.properties = PropertiesUtils.merge(defaultProperties,
					properties);
		} else {
			this.properties = defaultProperties;
		}
		initConfiguration(this.properties);
	}

	private void initConfiguration(Properties properties) {
		// 初始化ThreadPoolExecutor
		this.initThreadPoolExecutor(properties);
		// 初始化ForkJoinPool
		this.initForkJoinPool(properties);
	}

	private void initForkJoinPool(Properties properties) {
		String forkJoinPoolBean = properties.getProperty(FORKJOINPOOL_BEAN_KEY);
		if (forkJoinPoolBean != null)
			this.forkJoinPool = beanFactory.getBean(forkJoinPoolBean);
		if (forkJoinPool == null) {
			this.forkJoinPool = new ForkJoinPool();
		}
	}

	@SuppressWarnings("unchecked")
	private void initThreadPoolExecutor(Properties properties) {
		String threadPoolExecutorBean = properties
				.getProperty(THREADPOOLEXECUTOR_BEAN_KEY);
		if (threadPoolExecutorBean != null)
			this.threadPoolExecutor = beanFactory
					.getBean(threadPoolExecutorBean);
		if (this.threadPoolExecutor != null)
			return;
		String coolPoolSizeValue = properties
				.getProperty(THREADPOOLEXECUTOR_COREPOOLSIZE_KEY);
		int corePoolSize = Integer.parseInt(coolPoolSizeValue.trim());
		String maxPoolSizeValue = properties
				.getProperty(THREADPOOLEXECUTOR_MAXPOOLSIZE_KEY);
		int maxPoolSize = Integer.parseInt(maxPoolSizeValue.trim());
		String keepAliveTimeValue = properties
				.getProperty(THREADPOOLEXECUTOR_KEEPALIVETIME_KEY);
		long keepAliveTime = Long.parseLong(keepAliveTimeValue.trim());
		String unitValue = properties
				.getProperty(THEADPOOLEXECUTOR_TIMEUNIT_KEY);
		TimeUnit unit = TimeUnit.valueOf(unitValue.trim());
		BlockingQueue<Runnable> queue = null;
		String queueValue = properties
				.getProperty(THREADPOOLEXECUTOR_BLOCKQUEUE_TYPE_KEY);
		if (queueValue != null) {
			try {
				Class<?> queueType = Class.forName(queueValue.trim());
				queue = (BlockingQueue<Runnable>) queueType.newInstance();
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		}
		this.threadPoolExecutor = new ThreadPoolExecutor(corePoolSize,
				maxPoolSize, keepAliveTime, unit, queue);
	}

	public ThreadPoolExecutor getThreadPoolExecutor() {
		return threadPoolExecutor;
	}

	public ForkJoinPool getForkJoinPool() {
		return forkJoinPool;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setBeanFactory(BeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}
}
