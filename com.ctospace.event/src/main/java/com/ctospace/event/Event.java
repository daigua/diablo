package com.ctospace.event;

public class Event extends java.util.EventObject{

	private static final long serialVersionUID = -3817366937692125536L;

	/**
	 * source对象为由哪个类对象来触发的该事件。
	 * 如：awt界面上的按钮、 后台程序触发该事件的类
	 * */
	public Event(Object source) {
		super(source);
	}

}
