package com.ctospace.event;

import java.util.List;

public interface EventListenerLoader {
	
	public List<EventListener<? extends Event>> load();
	
}
