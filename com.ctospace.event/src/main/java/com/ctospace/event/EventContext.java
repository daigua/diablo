package com.ctospace.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.stereotype.Component;

@Component("com.ctospace.event.EventContext")
public class EventContext {

	private List<EventListenerLoader> eventListenerLoaders;

	private Map<Class<?>, List<EventListener<? extends Event>>> listenersContext;

	public EventContext() {
		this.eventListenerLoaders = new ArrayList<EventListenerLoader>();
		this.listenersContext = new HashMap<Class<?>, List<EventListener<? extends Event>>>();
	}

	@PostConstruct
	public void init() {
		for (EventListenerLoader eventListenerLoader : this.eventListenerLoaders) {
			List<EventListener<? extends Event>> eventListeners = eventListenerLoader
					.load();
			for (EventListener<? extends Event> eventListener : eventListeners) {
				this.addEventListener(eventListener);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends Event, R extends EventListener<?>> List<R> getEventListeners(
			Class<T> event) {
		return (List<R>) this.listenersContext.get(event);
	}

	public <T extends Event, R extends EventListener<T>> void addEventListener(
			R listener) {
		Class<? extends Event> eventType = listener.eventType();
		List<EventListener<? extends Event>> listeners = this.listenersContext
				.get(listener.eventType());
		if (listeners == null) {
			listeners = new ArrayList<EventListener<? extends Event>>();
			this.listenersContext.put(eventType, listeners);
		}
		listeners.add(listener);
	}

	public void setEventListenerLoaders(
			List<EventListenerLoader> eventListenerLoaders) {
		this.eventListenerLoaders = eventListenerLoaders;
	}

	@Resource(name = "com.ctospace.event.AnnotationEventListenerLoader")
	public void setAnnotationEventListenerLoader(
			AnnotationEventListenerLoader annotationEventListenerLoader) {
		this.eventListenerLoaders.add(annotationEventListenerLoader);
	}
}
