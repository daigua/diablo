package com.ctospace.event.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;

/**
 * if the method parameter's length is not one or the parameter's type is not
 * {@link Event}, user should use this annotation to manifest how to get the
 * value from Event by the getter method
 * */
@Target({PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface EventParam {

	String value();

}
