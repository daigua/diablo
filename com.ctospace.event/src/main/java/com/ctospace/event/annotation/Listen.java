package com.ctospace.event.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ctospace.event.NullEvent;

/**
 * listen the event if {@link Listen#value()}
 * 
 * */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Listen {
	
	Class<?> eventType() default NullEvent.class;
	
}
