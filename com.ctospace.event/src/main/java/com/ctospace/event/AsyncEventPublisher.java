package com.ctospace.event;

import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 异步事件发布器，对所有的事件都通过ThreadPoolExecutor来调度
 * */
public class AsyncEventPublisher implements EventPublisher {

	/**
	 * 异步通知事件的线程池
	 * */
	private ThreadPoolExecutor threadPoolExcutor;
	
	/**
	 * 通过该构造器构造出来的实例，需要通过{@link #setThreadPoolExcutor(ThreadPoolExecutor)}显示set一个ThreadPoolExecutor
	 * */
	public AsyncEventPublisher(){
		
	}
	
	/**
	 * 线程池构造器，如果threadPoolExcutor不为null时，异步调度则通过该ThreadPoolExecutor调度
	 * */
	public AsyncEventPublisher(ThreadPoolExecutor threadPoolExcutor) {
		this.threadPoolExcutor = threadPoolExcutor;
	}

	@Override
	public <T extends Event, R extends EventListener<T>> void publish(
			final T event, List<R> listeners) {
		for (final R listener : listeners) {
			Runnable runnable = new Runnable() {

				@Override
				public void run() {
					listener.handle(event);
				}

			};
			this.threadPoolExcutor.execute(runnable);
		}
	}

	public void setThreadPoolExcutor(ThreadPoolExecutor threadPoolExcutor) {
		this.threadPoolExcutor = threadPoolExcutor;
	}
}
