package com.ctospace.event;

public interface EventListener<T extends Event> extends java.util.EventListener{
	
	/**
	 * 同步处理事件
	 * @param event 需要处理的事件
	 * */
	public void handle(T event);
	
	/**
	 * The event type that to listen
	 * */
	public Class<T> eventType();

}
