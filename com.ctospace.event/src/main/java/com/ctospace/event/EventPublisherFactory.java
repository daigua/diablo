package com.ctospace.event;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.ctospace.core.spi.bean.BeanFactory;

@Component("com.ctospace.event.EventPublisherFactory")
public class EventPublisherFactory {

	@Resource(name = "com.ctospace.core.spi.bean.BeanFactory")
	private BeanFactory beanFactory;
	@Resource(name = "com.ctospace.event.Configuration")
	private Configuration configuration;

	public EventPublisher getEventPublisher(Mode mode) {
		switch (mode) {
		case SYNC:
			return new SyncEventPublisher();
		case ASYNC:
			/**
			 * 先到BeanFactory中找，如果没有找到，则自己new一个
			 * */
			AsyncEventPublisher asyncEventPublisher = beanFactory
					.getBean(AsyncEventPublisher.class);
			if (asyncEventPublisher != null)
				return asyncEventPublisher;
			return createAsyncEventPublisher();
		case AYSNC_FORK_JOIN:
			AsyncForkJoinEventPublisher publisher = beanFactory
					.getBean(AsyncForkJoinEventPublisher.class);
			if(publisher != null)
				return publisher;
			return createAsyncForkJoinEventPublisher();
		default :
			return null;
		}
	}

	private AsyncEventPublisher createAsyncEventPublisher() {
		AsyncEventPublisher publisher = new AsyncEventPublisher();
		// 取出默认的ThreadPoolExecutor
		publisher.setThreadPoolExcutor(configuration.getThreadPoolExecutor());
		return publisher;
	}
	
	private AsyncForkJoinEventPublisher createAsyncForkJoinEventPublisher(){
		AsyncForkJoinEventPublisher publisher = new AsyncForkJoinEventPublisher();
		publisher.setForkJoinPool(configuration.getForkJoinPool());
		return publisher;
	}
}
