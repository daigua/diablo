package com.ctospace.event;

import java.util.List;

/**
 * give the event ability to accept feedback
 * E event type,
 * T result type
 * */
public interface FeedbackAble<E extends Event,T> {
	
	public void add(Feedback<T> feedback);
	public List<Feedback<T>> getFeedbacks();
	
}
