package com.ctospace.event.test;

import com.ctospace.event.Event;

public class TestEvent extends Event {

	private static final long serialVersionUID = 1L;

	private int size;
	
	private int height;
	
	private String title;
	
	private boolean result;
	
	public TestEvent(Object source) {
		super(source);
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}
}
