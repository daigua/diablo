package com.ctospace.event.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ctospace.core.bean.annotation.Bean;
import com.ctospace.event.Feedback;
import com.ctospace.event.annotation.EventFeedback;
import com.ctospace.event.annotation.EventParam;
import com.ctospace.event.annotation.Listen;
import com.ctospace.event.annotation.Listener;

@Listener
@Bean
@Component
public class TestFeedbackEventListener {

	private Logger logger = LoggerFactory
			.getLogger(TestFeedbackEventListener.class);

	@Listen
	public void listen(TestFeedbackEvent event) {
		logger.info("test normal, the parameter is event. size : "
				+ event.getSize());
		logger.info("feedback");
		Feedback<Integer> feedback = new Feedback<Integer>();
		feedback.setResult(100);
		event.add(feedback);
	}

	@Listen(eventType=TestFeedbackEvent.class)
	@EventFeedback
	public int listen(int size) {
		logger.info("test parameter is event's attribute, size : " + size);
		return 200;
	}
	
	@Listen(eventType=TestFeedbackEvent.class)
	@EventFeedback
	public int listenParam(@EventParam("size") int sized){
		logger.info("test @EventParam, size : " + sized);
		return 300;
	}
}
