package com.ctospace.event.test;

import com.ctospace.event.FeedbackEvent;

public class TestFeedbackEvent extends FeedbackEvent<TestFeedbackEvent, Integer> {

	private static final long serialVersionUID = 1L;

	public TestFeedbackEvent(Object source) {
		super(source);
	}

	private int size;

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	
}
