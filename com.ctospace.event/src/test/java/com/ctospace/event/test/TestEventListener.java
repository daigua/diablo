package com.ctospace.event.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ctospace.core.bean.annotation.Bean;
import com.ctospace.event.annotation.EventParam;
import com.ctospace.event.annotation.Listen;
import com.ctospace.event.annotation.Listener;

@Listener
@Bean
@Component
public class TestEventListener {

	private Logger logger = LoggerFactory.getLogger(TestEventListener.class);
	
	@Listen
	public void listen(TestEvent testEvent){
		logger.info("test normal, the parameter is event.  title : " + testEvent.getTitle());
	}
	
	@Listen(eventType = TestEvent.class)
	public void listen(int size, int height , String title){
		logger.info("test parameter is event's attribute, title : "+title+", size : " + size + ", height : " + height);
	}
	@Listen(eventType = TestEvent.class)
	public void listenMix(int size, int height, @EventParam("title") String eventTitle){
		logger.info("test parameter is event's attribute, mix mode , title : "+eventTitle+", size : " + size + ", height : " + height);
	}
}
