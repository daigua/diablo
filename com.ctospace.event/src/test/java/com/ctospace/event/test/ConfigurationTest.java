package com.ctospace.event.test;

import java.util.Properties;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ctospace.event.Configuration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring.xml")
public class ConfigurationTest {
	
	@Resource
	private Configuration conf;
	
	@Test
	public void test(){
		Properties properties = conf.getProperties();
		Assert.assertEquals(properties.getProperty(Configuration.THREADPOOLEXECUTOR_COREPOOLSIZE_KEY), "10");
		Assert.assertEquals(properties.getProperty(Configuration.THREADPOOLEXECUTOR_MAXPOOLSIZE_KEY), "40");
		Assert.assertEquals(properties.getProperty(Configuration.THEADPOOLEXECUTOR_TIMEUNIT_KEY), "HOURS");
	}

}
