package com.ctospace.event.test;

import java.util.List;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ctospace.event.EventCenter;
import com.ctospace.event.Feedback;
import com.ctospace.event.Mode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring.xml")
public class EventCenterTest {
	
	@Resource
	private EventCenter eventCenter;
	
	private Logger logger = LoggerFactory.getLogger(EventCenter.class);
	
	@Test
	public void testEventListener(){
		TestEvent testEvent = new TestEvent(this);
		testEvent.setTitle("Test Event ");
		testEvent.setHeight(1000);
		testEvent.setSize(20000);
		logger.info("publish event by Normal mode");
		eventCenter.publish(testEvent);
		logger.info("publish event by Sync mode");
		eventCenter.publish(testEvent, Mode.ASYNC);
		logger.info("publish event by ForkJoin mode");
		eventCenter.publish(testEvent, Mode.AYSNC_FORK_JOIN);
	}
	@Test
	public void testFeedbackListener(){
		TestFeedbackEvent feedbackEvent = new TestFeedbackEvent(this);
		feedbackEvent.setSize(1000000);
		logger.info("publish event by Normal mode");
		eventCenter.publish(feedbackEvent);
		
		List<Feedback<Integer>> feedbacks = feedbackEvent.getFeedbacks();
		Assert.assertEquals(3, feedbacks.size());
		for(Feedback<Integer> feedback : feedbacks){
			logger.info("feedback : " + feedback.getResult());
			int result = feedback.getResult();
			assertFeedbackResult(result);
		}
		logger.info("publish event by Sync mode");
		feedbackEvent = new TestFeedbackEvent(this);
		feedbackEvent.setSize(1000000);
		try{
			eventCenter.publish(feedbackEvent, Mode.ASYNC);
		}catch(Exception e){
			logger.info("",e);
		}
		feedbackEvent = new TestFeedbackEvent(this);
		feedbackEvent.setSize(1000000);
		logger.info("publish event by ForkJoin mode");
		eventCenter.publish(feedbackEvent, Mode.AYSNC_FORK_JOIN);
		feedbacks = feedbackEvent.getFeedbacks();
		Assert.assertEquals(3, feedbacks.size());
		for(Feedback<Integer> feedback : feedbacks){
			logger.info("feedback : " + feedback.getResult());
			int result = feedback.getResult();
			assertFeedbackResult(result);
		}
	}
	
	private void assertFeedbackResult(int result){
		if(result != 100 && result != 200 && result != 300){
			throw new RuntimeException("test error");
		}
	}

}
